::
:: Для запуска этого скрипта, нужно создать переменную среды PHP_DIR, которая будет указывать на директорию с php интерпретатором
:: Брать отсюда https://windows.php.net/download/ (x64 Non Thread Safe)
::
@ECHO OFF
ECHO FPM UP!
set PATH=%PHP_DIR%;%PATH%
%PHP_DIR%\php-cgi.exe -b localhost:9123 -c %PHP_DIR%\php.ini