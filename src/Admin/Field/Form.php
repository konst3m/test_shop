<?php

declare(strict_types = 1);

namespace App\Admin\Field;

use App\Exception\InternalException;
use Carbon\CarbonImmutable;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\Form\Type\DateTimePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;

class Form
{

    public static function entityAutoComplete(
        string $fieldName,
        string $modelClass,
        string $label,
        callable $fieldCallback = null,
        string $route = null,
        string $adminCode = null,
        string $placeholder = '',
        bool $required = true,
        bool $compound = true,
        bool $mapped = true,
        string $help = null,
        string|callable $callback = null,
        callable $getter = null,
        array $reqParams = null,
        bool $multiple = false,
        callable $choiceLabel = null,
        string $admin = null,
        string|array $property = 'id',
    ): array {
        $params = [
            'class'              => $modelClass,
            'property'           => $property,
            'label'              => $label,
            'placeholder'        => $placeholder,
            'to_string_callback' => $choiceLabel ?? $fieldCallback,
            'btn_add'            => false,
            'callback'           => $callback,
            'getter'             => $getter,
            'required'           => $required,
            'compound'           => $compound,
            'mapped'             => $mapped,
            'help'               => $help,
            'req_params'         => $reqParams,
            'multiple'           => $multiple,
        ];

        if ($route) {
            $params['route'] = ['name' => $route];
        }

        return [
            $fieldName,
            ModelAutocompleteType::class,
            $params,
            ['admin_code' => $admin ?? $adminCode],
        ];
    }

    public static function entity(
        string $fieldName,
        string $entityClass,
        string $label,
        callable|string $choiceLabel = null,
        callable|string $choiceValue = 'id',
        callable $query = null,
        callable $setter = null,
        bool $required = true,
        string $admin = null,
        ?string $help = null,
        bool $mapped = true,
    ): array {
        return [
            $fieldName,
            EntityType::class,
            [
                'label'         => $label,
                'class'         => $entityClass,
                'choice_label'  => $choiceLabel,
                'choice_value'  => $choiceValue,
                'query_builder' => $query,
                'setter'        => $setter,
                'required'      => $required,
                'help'          => $help,
                'mapped'        => $mapped,
            ],
            ['admin_code' => $admin],
        ];
    }

    public static function number(
        string $filed,
        string $label,
        int $scale = null,
        string $help = null,
        int|float $data = null,
        bool $required = true,
        bool $mapped = true,
        string $propertyPath = null,
        callable $setter = null,
    ): array {
        $result = [
            $filed,
            NumberType::class,
            [
                'label'         => $label,
                'required'      => $required,
                'property_path' => $propertyPath,
                'mapped'        => $mapped,
                'scale'         => $scale,
                'help'          => $help,
                'setter'        => $setter,
            ],
        ];

        if (isset($data)) {
            $result[2]['data'] = $data;
        }

        return $result;
    }

    public static function text(
        string $field,
        string $label,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
        string $propertyPath = null,
        callable $setter = null,
        callable $getter = null,
        string $admin = null,
        bool $disabled = false,
        string $data = null,
        array $attr = [],
    ): array {
        $params = [
            'label'         => $label,
            'required'      => $required,
            'property_path' => $propertyPath,
            'mapped'        => $mapped,
            'help'          => $help,
            'setter'        => $setter,
            'getter'        => $getter,
            'disabled'      => $disabled,
            'attr'          => $attr,
        ];
        if ($data) {
            $params['data'] = $data;
        }

        return [
            $field,
            TextType::class,
            $params,
            ['admin_code' => $admin],
        ];
    }

    public static function textarea(
        string $field,
        string $label,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
        string $admin = null,
        bool $disabled = false,
        string $data = null,
        array $attr = [],
    ): array {
        $params = [
            'label'    => $label,
            'required' => $required,
            'mapped'   => $mapped,
            'help'     => $help,
            'disabled' => $disabled,
            'attr'     => $attr,
        ];
        if ($data) {
            $params['data'] = $data;
        }

        return [
            $field,
            TextareaType::class,
            $params,
            ['admin_code' => $admin],
        ];
    }

    public static function choice(
        string $field,
        string $label,
        array $chose,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
        callable $choiceLabel = null,
        bool $multiple = false,
        bool $disabled = false,
    ): array {
        return [
	        $field,
            ChoiceType::class,
            [
                'label'        => $label,
                'choices'      => $chose,
                'required'     => $required,
                'mapped'       => $mapped,
                'help'         => $help,
                'choice_label' => $choiceLabel,
	            'multiple'     => $multiple,
	            'disabled'     => $disabled,
            ],
        ];
    }

    public static function datetime(
        string $field,
        string $label,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
    ): array {
        /*return [
            $field,
            DateTimeType::class,
            [
                'label'        => $label,
                'date_widget'  => $dateWidget,
                'with_minutes' => $withMinutes,
                'required'     => $required,
                'mapped'       => $mapped,
                'help'         => $help,
            ],
        ];*/

        return [
            $field,
            DateTimePickerType::class,
            [
                'label'          => $label,
                'dp_use_seconds' => false,
                'format'         => 'yyyy-MM-dd',
                'required'       => $required,
                'mapped'         => $mapped,
                'help'           => $help,
            ],
        ];
    }

    public static function date(
        string $field,
        string $label,
        string $widget = 'choice',
        array $days = null,
        array $years = null,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
    ): array {
        $now = CarbonImmutable::now();

        $result = [
            $field,
            DateType::class,
            [
                'label'    => $label,
                'widget'   => $widget,
                'required' => $required,
                'mapped'   => $mapped,
                'help'     => $help,
                'data'     => $now,
            ],
        ];

        if (isset($years)) {
            $result[2]['years'] = $years;
        }

        if (isset($days)) {
            $result[2]['days'] = $days;
        }

        return $result;
    }

    public static function file(
        string $field,
        string $label,
        ?array $mimeTypes = ['application/x-pdf'],
        string $mimeTypesMessage = 'Ожидается pdf документ',
        bool $multiple = false,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
    ): array {
        return [
            $field,
            FileType::class,
            [
                'label'       => $label,
                'required'    => $required,
                'mapped'      => $mapped,
                'multiple'    => $multiple,
                'help'        => $help,
                'constraints' => [
                    new File([
                        'mimeTypes'        => $mimeTypes,
                        'mimeTypesMessage' => $mimeTypesMessage,
                    ]),
                ],
            ],
        ];
    }

    public static function file2(
        string $field,
        string $label,
        array $constraints = [],
        bool $multiple = false,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
    ): array {
        return [
            $field,
            FileType::class,
            [
                'label'       => $label,
                'required'    => $required,
                'mapped'      => $mapped,
                'help'        => $help,
                'multiple'    => $multiple,
                'constraints' => $constraints,
            ],
        ];
    }

    public static function checkbox(
        string $field,
        string $label,
        bool $checked = false,
        bool $required = false,
        bool $mapped = true,
        string $help = null,
    ): array {
        return [
            $field,
            CheckboxType::class,
            [
                'label'    => $label,
                'value'    => '1',
                'required' => $required,
                'help'     => $help,
                'mapped'   => $mapped,
                // data нельзя использовать, потому что для редактирования будет использоваться всегда false
                // даже если там true
                //'data'     => null,
            ],
        ];
    }

    public static function enum(
        string $filed,
        string $label,
        string $enumClass,
        string $help = null,
        bool $required = true,
        bool $mapped = true,
        array $attr = [],
        string|callable $choiceLabel = null,
    ): array {
        if (!enum_exists($enumClass)) {
            throw new InternalException('Ожидается enum');
        }

        $params = [
            'class'    => $enumClass,
            'label'    => $label,
            'required' => $required,
            'mapped'   => $mapped,
            'help'     => $help,
            'attr'     => $attr,
        ];
        if ($choiceLabel) {
            $params['choice_label'] = $choiceLabel;
        }

        return [$filed, EnumType::class, $params];
    }
}
