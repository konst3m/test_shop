<?php

declare(strict_types = 1);

namespace App\Admin\Field;

use App\Value\Amount\Amount;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\FieldDescription\FieldDescriptionInterface;

class Show
{

    public static function relations(
        string $fieldName,
        string $label,
        string|callable $property,
        array $route = null,
        string $relationType = FieldDescriptionInterface::TYPE_MANY_TO_ONE,
        string $adminClass = null,
        bool $sortable = false,
        callable $accessor = null,
    ): array {
        return [
            $fieldName,
            $relationType,
            [
                'label'                            => $label,
                'associated_property'              => $property,
                'admin_code'                       => $adminClass,
                'route'                            => $route,
                'sortable'                         => $sortable,
                'accessor'                         => $accessor,
                'sort_parent_association_mappings' => array_map(
                    fn($e) => ['fieldName' => $e],
                    explode('.', $fieldName),
                ),
            ],
        ];
    }

    public static function actions(array $actions, string $label = 'Действия'): array
    {
        return [
            ListMapper::NAME_ACTIONS,
            ListMapper::TYPE_ACTIONS,
            [
                'label'   => $label,
                'actions' => $actions,
            ],
        ];
    }

    public static function string(
        string $fieldName,
        string $label,
        callable $accessor = null,
        bool $sortable = true,
    ): array {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_STRING,
            [
                'label'    => $label,
                'accessor' => $accessor,
                'sortable' => $sortable,
            ],
        ];
    }

    public static function int(
        string $fieldName,
        string $label,
        bool $sortable = true,
        callable $accessor = null,
    ): array {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_INTEGER,
            [
                'label'    => $label,
                'sortable' => $sortable,
                'accessor' => $accessor,
            ],
        ];
    }

    public static function float(string $fieldName, string $label, bool $sortable = true): array
    {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_FLOAT,
            [
                'label'    => $label,
	            'sortable' => $sortable,
            ],
        ];
    }

    public static function id(
        string $fieldName = 'id',
        string $label = 'ID',
        callable $accessor = null,
        bool $sortable = true,
    ): array {
        return [
            $fieldName,
            null,
            [
                'identifier' => true,
                'label'      => $label,
                'accessor'   => $accessor,
                'sortable'   => $sortable,
            ],
        ];
    }

    public static function enum(string $fieldName, string $label): array
    {
        return self::string("$fieldName.value", $label);
    }

    public static function choice(string $fieldName, string $label, array $choice, bool $sortable = true): array
    {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_CHOICE,
            [
                'label'    => $label,
                'choices'  => $choice,
                'sortable' => $sortable,
            ],
        ];
    }

    public static function array(string $fieldName, string $label, callable $accessor = null): array
    {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_ARRAY,
            [
                'label'    => $label,
                'accessor' => $accessor,
            ],
        ];
    }

    public static function datetime(string $fieldName, string $label, string $format = 'd.m.Y H:i', bool $sortable = true): array
    {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_DATETIME,
            [
                'label'    => $label,
                'format'   => $format,
                'sortable' => $sortable,
            ],
        ];
    }

    public static function bool(string $fieldName, string $label): array
    {
        return [
            $fieldName,
            FieldDescriptionInterface::TYPE_BOOLEAN,
            [
                'label' => $label,
            ],
        ];
    }

    public static function amount(
        string $fieldName, string $label, callable $fieldAccessor, bool $currency = true, bool $sortable = true,
    ): array
    {
        return self::string(
            $fieldName,
            $label,
            accessor: function (object $object) use ($fieldAccessor, $currency) {
                /** @var Amount $amount */
                $amount = $fieldAccessor($object);

                $value = $amount?->getWithVAT();
                if (!$value) {
                    return null;
                }

                if ($currency) {
                    $value = $amount->getCurrency()->value . ' ' . $value;
                }

                return $value;
            },
            sortable: $sortable,
        );
    }
}