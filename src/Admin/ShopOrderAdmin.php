<?php
declare(strict_types = 1);

namespace App\Admin;

use App\Admin\Field\Form;
use App\Admin\Field\Show;
use App\Entity\Enum\OrderStatus;
use App\Exception\InternalException;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ShopOrderAdmin extends AbstractAdmin
{

    /**
     * @throws InternalException
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add(...Form::enum('orderStatus', OrderStatus::class, OrderStatus::class));
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->add(...Show::enum('orderStatus', 'Статус'))
            ->add(...Show::enum('deliveryType', 'Тип доставки'))
            ->add(...Show::string('phone', 'Телефон на заказе'))
            ->add(...Show::datetime('createdAt', 'Создан'))
            ->add(...Show::datetime('updatedAt', 'Обновлен'));
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add(...Show::enum('deliveryType', 'Тип доставки'))
            ->add(...Show::string('phone', 'Телефон на заказе'))
            ->add(...Show::datetime('createdAt', 'Создан'))
            ->add(...Show::datetime('updatedAt', 'Обновлен'));
    }
}