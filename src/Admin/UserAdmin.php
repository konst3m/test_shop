<?php
declare(strict_types = 1);

namespace App\Admin;

use App\Admin\Field\Show;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

class UserAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('id')
            ->add('email');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->addIdentifier('id')
            ->add(...Show::string('phone', 'Телефон'))
            ->add(...Show::string('email', 'Почта'))
            ->add(...Show::bool('blocked', 'Заблокирован'));
    }
}