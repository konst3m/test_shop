<?php

declare(strict_types = 1);

namespace App\Controller\Admin;

use App\Dictionary\RouteName;
use App\Form\AdminLoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route(path: '/admin', methods: ['GET', 'POST'])]
class SecurityController extends AbstractController
{
	#[Route(path: '/', name: RouteName::ROUTE_ADMIN, )]
	public function index(): Response
	{
		return new RedirectResponse($this->generateUrl('admin_login'), 301);
	}

	#[Route(path: '/login', name: RouteName::ROUTE_ADMIN_LOGIN)]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $form = $this->createForm(AdminLoginForm::class, [
            'email' => $authenticationUtils->getLastUsername(),
        ]);

        return $this->render('security/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'form'          => $form->createView(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }
}