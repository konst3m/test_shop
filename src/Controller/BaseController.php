<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\User;

use App\Helper\ResponseHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Service\Attribute\Required;

class BaseController extends AbstractController
{

    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED  = 'failed';
    private NormalizerInterface $normalizer;

    #[Required]
    public function initialize(NormalizerInterface $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    protected function getUser(): User
    {
        /** @var User $user */
        if (($user = parent::getUser()) instanceof User) {
            return $user;
        }

        throw new UnauthorizedHttpException('Basic');
    }

    /**
     * Сформировать API ответ
     *
     * @param mixed $data
     * @param array $context
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    protected function buildApiResponse(mixed $data = [], array $context = []): JsonResponse
    {
        $context[Serializer::EMPTY_ARRAY_AS_OBJECT] ??= false;

        return ResponseHelper::jsonResponse([
            'status' => self::STATUS_SUCCESS,
            'data'   => $this->normalizer->normalize($data, context: $context),
        ]);
    }

    /**
     * Возвращает API ошибку
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function buildApiError(string $message): JsonResponse
    {
        return ResponseHelper::jsonResponse([
            'status' => self::STATUS_FAILED,
            'code'   => 0,
            'error'  => [
                'message' => $message,
            ],
        ]);
    }
}
