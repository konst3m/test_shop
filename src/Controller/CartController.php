<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Dictionary\RouteName;
use App\Exception\Core\JsonException;
use App\Exception\InternalException;
use App\Helper\Json;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTOExtractor\BuyCartDTOExtractor;
use App\Http\RequestDTOExtractor\CartDTOExtractor;
use App\Service\CartService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

#[Route(path: '/api', methods: 'POST')]
class CartController extends BaseController
{

    public function __construct(
        private readonly CartService $cartService,
        private readonly UserService $userService,
    ) {
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/get-cart', name: RouteName::ROUTE_GET_CART)]
    public function getCart(): JsonResponse
    {
        return $this->buildApiResponse($this->cartService->getOrCreateCart($this->userService->getUser()));
    }

    /**
     * @throws RequestValidationException
     * @throws InternalException
     * @throws ExceptionInterface
     */
    #[Route('/add-to-cart', name: RouteName::ROUTE_ADD_TO_CART)]
    public function addToCart(CartDTOExtractor $extractor): Response
    {
        return $this->buildApiResponse(
            $this->cartService->addToCart($extractor->extract()),
        );
    }

    /**
     * @throws RequestValidationException
     * @throws InternalException
     * @throws ExceptionInterface
     */
    #[Route('/remove-from-cart', name: RouteName::ROUTE_REMOVE_FROM_CART)]
    public function removeToCart(CartDTOExtractor $extractor): Response
    {
        return $this->buildApiResponse(
            $this->cartService->removeFromCart($extractor->extract()),
        );
    }

    /**
     * @throws RequestValidationException
     * @throws InternalException
     */
    #[Route('/buy-cart', name: RouteName::ROUTE_BUY_CART)]
    public function buyCart(BuyCartDTOExtractor $extractor): Response
    {
        $this->cartService->buyCart($extractor->extract());

        return new Response('OK');
    }
}