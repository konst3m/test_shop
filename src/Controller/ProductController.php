<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Dictionary\RouteName;
use App\Exception\InternalException;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTOExtractor\ProductDTOExtractor;
use App\Service\ProductService;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

#[Route(path: '/api')]
class ProductController extends BaseController
{
    public function __construct(
        private readonly ProductService $productService,

    ) { }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/list-product', name: RouteName::ROUTE_LIST_PRODUCT, methods: 'POST')]
    public function listProduct(ProductDTOExtractor $extractor): JsonResponse
    {
        return $this->buildApiResponse($this->productService->getList());
    }

    /**
     * @throws RequestValidationException
     * @throws DivisionByZeroException
     * @throws RoundingNecessaryException
     * @throws InternalException
     * @throws NumberFormatException
     * @throws ExceptionInterface
     */
    #[Route('/create-product', name: RouteName::ROUTE_CREATE_PRODUCT, methods: 'POST')]
    public function createProduct(ProductDTOExtractor $extractor): JsonResponse
    {
        $product = $this->productService->createProduct($extractor->extract());
        return $this->buildApiResponse($product);
    }

    /**
     * @throws RequestValidationException
     * @throws DivisionByZeroException
     * @throws RoundingNecessaryException
     * @throws InternalException
     * @throws NumberFormatException
     */
    #[Route('/update-product', name: RouteName::ROUTE_UPDATE_PRODUCT, methods: 'POST')]
    public function updateProduct(ProductDTOExtractor $extractor): Response
    {
        $product = $this->productService->updateProduct($extractor->extract());
        return new Response('test');
    }
}