<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Dictionary\RouteName;
use App\Event\ReportGeneratedEvent;
use App\Exception\Core\JsonException;
use App\Helper\Json;
use App\Service\ReportOrderService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route(path: '/api', methods: 'POST')]
class ReportController extends BaseController
{

    public function __construct(
        private readonly ReportOrderService $reportOrderService,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {
    }

    /**
     * @throws JsonException
     */
    #[Route('/create-report', name: RouteName::ROUTE_CREATE_REPORT)]
    public function getCart(): JsonResponse
    {
        $projectRoot = $this->getParameter('kernel.project_dir');

        try {
            $this->eventDispatcher->dispatch(
                new ReportGeneratedEvent(
                    $this->reportOrderService->generateReport($projectRoot)
                ),
            );
        } catch (\Throwable $exception) {
        }

        return new JsonResponse(Json::encode([]));
    }

}