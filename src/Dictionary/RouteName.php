<?php
declare(strict_types = 1);

namespace App\Dictionary;

class RouteName
{

    public const ROUTE_ADMIN            = 'admin';
    public const ROUTE_ADMIN_LOGIN      = 'admin_login';
    public const ROUTE_LIST_PRODUCT     = 'list-product';
    public const ROUTE_CREATE_PRODUCT   = 'create-product';
    public const ROUTE_UPDATE_PRODUCT   = 'update-product';
    public const ROUTE_ADD_TO_CART      = 'add_to_cart';
    public const ROUTE_REMOVE_FROM_CART = 'remove_from_cart';
    public const ROUTE_GET_CART         = 'get_cart';
    public const ROUTE_BUY_CART         = 'buy_cart';
    public const ROUTE_CREATE_REPORT    = 'create-report';
}