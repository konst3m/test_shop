<?php

declare(strict_types = 1);

namespace App\Dictionary;

class Size
{

    /** @var int Байт */
    public const BYTE = 1;
    /** @var int Килобайт */
    public const KB   = self::BYTE * 1024;
    /** @var int Мегабайт */
    public const MB   = self::KB * 1024;
    /** @var int Гигабайт */
    public const GB   = self::MB * 1024;
}