<?php
declare(strict_types = 1);

namespace App\Doctrine\Type;

use Carbon\CarbonImmutable;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class CarbonImmutableDateTimeType extends Type
{
    const CARBON_IMMUTABLE_DATETIME = 'carbon_immutable_datetime';

    public function getName(): string
    {
        return self::CARBON_IMMUTABLE_DATETIME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?CarbonImmutable
    {
        if ($value === null || $value instanceof CarbonImmutable) {
            return $value;
        }

        return CarbonImmutable::createFromFormat($platform->getDateTimeFormatString(), $value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof CarbonImmutable) {
            return $value->format($platform->getDateTimeFormatString());
        }

        throw new \InvalidArgumentException('Invalid type for CarbonImmutableDateTimeType');
    }

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'DATETIME';
    }
}