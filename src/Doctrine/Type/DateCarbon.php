<?php

declare(strict_types = 1);

namespace App\Doctrine\Type;

class DateCarbon extends CarbonImmutableDateTimeType
{

    public function getName(): string
    {
        return 'date';
    }
}