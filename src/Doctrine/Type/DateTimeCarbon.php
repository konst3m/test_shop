<?php

declare(strict_types = 1);

namespace App\Doctrine\Type;


class DateTimeCarbon extends CarbonImmutableDateTimeType
{

    public function getName(): string
    {
        return 'datetime';
    }
}