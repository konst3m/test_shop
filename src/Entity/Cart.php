<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Traits\TimestampTrait;
use App\Traits\WithAmountTrait;
use App\Traits\WithIdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CartRepository::class)]
class Cart
{

    use WithIdTrait;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    public User $user;

    #[ORM\OneToMany(targetEntity: ShopOrder::class, mappedBy: 'cart')]
    public Collection $orders;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->orders = new ArrayCollection();
    }

    public function addShopOrder(ShopOrder $order): self {
        if(!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->cart = $this;
        }

        return $this;
    }
}