<?php

declare(strict_types = 1);

namespace App\Entity\Enum;

enum DeliveryType: string
{
    case COURIER = 'courier';
    case PICKUP = 'pickup';
}