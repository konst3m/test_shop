<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Repository\ProductRepository;
use App\Traits\TimestampTrait;
use App\Traits\WithAmountTrait;
use App\Traits\WithIdTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{

    use TimestampTrait;
    use WithIdTrait;
    use WithAmountTrait;

    #[ORM\Column(type: Types::STRING, length: 255)]
    public string $name;

    #[ORM\Column(type: Types::TEXT)]
    public string $description;

    #[ORM\Column(type: Types::INTEGER)]
    public int $version;

    #[ORM\Column(type: Types::FLOAT)]
    public float $weight;

    #[ORM\Column(type: Types::FLOAT)]
    public float $tax;

    #[ORM\Column(type: Types::FLOAT)]
    public float $height;

    #[ORM\Column(type: Types::FLOAT)]
    public float $width;

    #[ORM\Column(type: Types::FLOAT)]
    public float $length;

    #[ORM\ManyToMany(targetEntity: ShopOrder::class, mappedBy: 'products')]
    private Collection $orders;
}