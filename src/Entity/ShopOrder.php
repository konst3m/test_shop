<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Enum\DeliveryType;
use App\Entity\Enum\OrderStatus;
use App\EventSubscriber\EntityListener\ShopOrderListener;
use App\Repository\ShopOrderRepository;
use App\Traits\TimestampTrait;
use App\Traits\WithIdTrait;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShopOrderRepository::class)]
#[ORM\EntityListeners([ShopOrderListener::class])]
#[ORM\HasLifecycleCallbacks]
class ShopOrder
{

    use TimestampTrait;
    use WithIdTrait;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    public User $user;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'orders')]
    public Collection $products;

    #[ORM\ManyToOne(targetEntity: Cart::class, inversedBy: 'orders')]
    public Cart $cart;

    #[ORM\Column(type: 'string', length: 255, enumType: OrderStatus::class)]
    public OrderStatus $orderStatus = OrderStatus::NEW;

    #[ORM\Column(type: 'string', length: 255, nullable: true, enumType: DeliveryType::class)]
    public ?DeliveryType $deliveryType;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    public ?string $phone;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    public ?string $reportNumber;

    #[ORM\Column(name: 'reported_at', type: 'datetime', nullable: true)]
    public ?CarbonImmutable $reporterAt;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->products = new ArrayCollection();
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

}