<?php

declare(strict_types = 1);

namespace App\Entity;


use App\EventSubscriber\EntityListener\UserListener;

use App\Repository\UserRepository;
use App\Traits\TimestampTrait;
use App\Traits\WithIdTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\EntityListeners([UserListener::class])]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    use TimestampTrait;
    use WithIdTrait;

    public const SYSTEM_USER_ID = -1;

    public const ROLES_WITH_ADMIN = [
        self::ROLE_ADMIN,
    ];

    public const ROLE_API   = 'ROLE_API';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    public const ROLE_CAPTIONS = [
        self::ROLE_API   => 'Пользователь',
        self::ROLE_ADMIN => 'Администратор',
    ];

    #[ORM\Column(type: Types::STRING, length: 255)]
    public string $name;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
    public string $email;

    #[ORM\Column(type: Types::STRING, length: 255)]
    public string $password = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    public ?string $phone = null;

    #[ORM\Column(type: 'array')]
    public array $roles = [];

    #[ORM\Column(type: Types::BOOLEAN)]
    public bool $phoneValidated = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    public bool $emailValidated = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    public bool $blocked = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    public bool $deleted = false;

    #[ORM\Column(type: Types::STRING, length: 32, unique: true, nullable: false)]
    public string $apiKey;

    #[ORM\Column(type: Types::STRING, length: 32, nullable: true)]
    public ?string $apiSecret = null;


    /** @throws Exception */
    public function __construct()
    {
        $this->apiKey = bin2hex(random_bytes(16));
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function setPassword(string $hashPassword): void
    {
        $this->password = $hashPassword;
    }
}