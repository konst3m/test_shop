<?php
declare(strict_types = 1);

namespace App\Event;

use App\Entity\ShopOrder;
use Symfony\Contracts\EventDispatcher\Event;

class OrderNotificationEvent extends Event
{

    public function __construct(
        public readonly ShopOrder $shopOrder,
    ) {
    }

}