<?php
declare(strict_types = 1);

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ReportGeneratedEvent extends Event
{

    public function __construct(
        public readonly string $reportNumber,
    ) {
    }

}