<?php

declare(strict_types = 1);

namespace App\EventSubscriber\EntityListener;

use App\Entity\Enum\OrderStatus;
use App\Entity\ShopOrder;
use App\Event\OrderNotificationEvent;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\EventDispatcher\EventDispatcherInterface;

class ShopOrderListener
{

    public OrderStatus|null $oldStatus = null;
    public OrderStatus|null $newStatus = null;

    private const NOTIFICATION_STATUSES = [
        OrderStatus::DONE,
        OrderStatus::NEW,
        OrderStatus::PAID,
    ];

    public function __construct(
        private readonly EventDispatcherInterface $dispatcher,
    ) {
    }

    public function postUpdate(ShopOrder $shopOrder): void
    {
        if ($this->oldStatus !== null) {
            $this->newStatus = $shopOrder->orderStatus;

            if ($this->oldStatus !== $this->newStatus) {
                if (in_array($this->newStatus, self::NOTIFICATION_STATUSES, true)) {
                    $this->dispatcher->dispatch(new OrderNotificationEvent($shopOrder));
                }
            }
        }
    }

    public function preUpdate(ShopOrder $shopOrder, PreUpdateEventArgs $eventArgs): void
    {
        if ($eventArgs->hasChangedField('orderStatus')) {
            $this->oldStatus = OrderStatus::from($eventArgs->getOldValue('orderStatus'));
        }
    }
}