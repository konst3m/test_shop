<?php

declare(strict_types = 1);

namespace App\EventSubscriber\EntityListener;

use App\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class UserListener
{

    public function __construct()
    {
    }

    public function postUpdate(User $user): void
    {
    }

    public function preUpdate(User $user, PreUpdateEventArgs $eventArgs): void
    {
    }
}