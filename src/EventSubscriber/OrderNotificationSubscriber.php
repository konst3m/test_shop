<?php
declare(strict_types = 1);

namespace App\EventSubscriber;

use App\Event\OrderNotificationEvent;
use App\Notification\NotificationProcessor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderNotificationSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private readonly NotificationProcessor $notificationProcessor,
    ) { }

    public static function getSubscribedEvents(): array
    {
        return [
            OrderNotificationEvent::class => 'sendNotification',
        ];
    }

    public function sendNotification(OrderNotificationEvent $event):void {
        $this->notificationProcessor->sendNotification($event);
    }
}