<?php
declare(strict_types = 1);

namespace App\EventSubscriber;

use App\Event\ReportGeneratedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ReportGeneratedSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            ReportGeneratedEvent::class => 'sendToQueue',
        ];
    }

    public function sendToQueue(ReportGeneratedEvent $event):void {
        // тут будем досылать в кафку по имени файла
        $x = $event;
    }
}