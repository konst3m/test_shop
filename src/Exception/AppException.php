<?php

declare(strict_types = 1);

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Корневое исключение
 */
class AppException extends Exception
{

    /**
     * Оборачивает исключение в текущий класс исключения
     *
     * @param Throwable $throwable
     * @return static
     */
    public static function wrap(Throwable $throwable): static
    {
        return new static($throwable->getMessage(), $throwable->getCode(), $throwable);
    }

    /** @deprecated */
    public static function new(string $message = '', int $code = 0, Throwable $previous = null): static
    {
        return new static(...func_get_args());
    }

    /**
     * Делает вызов функции и оборачивает исключение, если оно возникает
     *
     * @param callable $callback
     * @param array    $args
     * @return mixed
     * @throws static
     */
    public static function tryIt(callable $callback, array $args = []): static
    {
        try {
            return call_user_func_array($callback, $args);
        } catch (Throwable $throwable) {
            throw static::wrap($throwable);
        }
    }

    /** @throws static */
    public static function throw(string $message = '', int $code = 0, Throwable $previous = null): void
    {
        throw static::new(...func_get_args());
    }
}