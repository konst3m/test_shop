<?php

declare(strict_types = 1);

namespace App\Exception\Core;

use App\Exception\InternalException;

/**
 * Если сущность не найдена
 */
class EntityNotFound extends InternalException
{

    public function __construct(
        public readonly string $entityClass,
    )
    {
        parent::__construct("Сущность не найдена: $entityClass");
    }
}