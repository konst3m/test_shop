<?php

declare(strict_types = 1);

namespace App\Exception\Core;

use App\Exception\InternalException;

class JsonException extends InternalException
{

}