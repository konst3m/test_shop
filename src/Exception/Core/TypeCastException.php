<?php

declare(strict_types = 1);

namespace App\Exception\Core;

/**
 * При возникновении проблемы с типами
 */
class TypeCastException extends TypeException
{

    public function __construct(
        string $expectedType,
        string $receivedType,
    )
    {
        parent::__construct("Ошибка каста типа: попытка кастовать из {$receivedType} в {$expectedType}");
    }
}