<?php

declare(strict_types = 1);

namespace App\Exception\Core;

use App\Exception\InternalException;

/**
 * При возникновении проблемы с типами
 */
class TypeException extends InternalException
{

}