<?php

declare(strict_types = 1);

namespace App\Exception\Core;

use App\Exception\InternalException;

/**
 * Ошибка валидации чего-либо
 */
class ValidationException extends InternalException
{

}