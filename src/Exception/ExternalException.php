<?php

declare(strict_types = 1);

namespace App\Exception;

/**
 * Исключение, вызванное проблемами, связанные с внешними системами
 */
class ExternalException extends AppException
{

}