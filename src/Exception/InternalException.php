<?php

declare(strict_types = 1);

namespace App\Exception;

/**
 * Исключение, вызванное внутренними проблемами
 */
class InternalException extends AppException
{

}