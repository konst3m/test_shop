<?php

declare(strict_types = 1);

namespace App\Exception;

/**
 * Неожиданный null
 */
class NullException extends InternalException
{

    public function __construct(
        $message = 'Неожиданный null',
        $code = 0,
        \Throwable $previous = null,
    )
    {
        parent::__construct(...func_get_args());
    }
}