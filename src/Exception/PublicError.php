<?php

declare(strict_types = 1);

namespace App\Exception;

use Throwable;

/**
 * Публичная ошибка.
 * Только эти исключения могут отображаться клиентам
 */
interface PublicError extends Throwable
{

    public function getPublicMessage(): string;

    public function getPublicData(): array;
}