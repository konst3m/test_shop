<?php

declare(strict_types = 1);

namespace App\Helper;

use App\Exception\Core\JsonException;
use GuzzleHttp\Exception\InvalidArgumentException;
use GuzzleHttp\Utils;

class Json
{

    /** @throws JsonException */
    public static function encode(mixed $value, int $options = 0, int $depth = 512): string
    {
        try {
            return Utils::jsonEncode($value, $options, $depth);
        } catch (InvalidArgumentException $exception) {
            throw JsonException::wrap($exception);
        }
    }

    /** @throws JsonException */
    public static function decode(string $json, bool $assoc = true, int $depth = 512, int $options = 0): mixed
    {
        try {
            return Utils::jsonDecode($json, $assoc, $depth, $options);
        } catch (InvalidArgumentException $exception) {
            throw JsonException::wrap($exception);
        }
    }

    public static function isValid(string $value): bool
    {
        try {
            self::decode($value);

            return true;
        } catch (JsonException) {
            return false;
        }
    }
}