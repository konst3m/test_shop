<?php

declare(strict_types = 1);

namespace App\Helper;

class NDS
{

    /** @var float 20% НДС */
    public const NDS_MULTIPLICATOR = 1.2;

    /**
     * Вычитает НДС из $amount
     *
     * @param float $amount
     * @return float
     */
    public static function subtractNDS(float $amount): float
    {
        return round($amount / self::NDS_MULTIPLICATOR, 2);
    }

    /**
     * Добавляет НДС к $amount
     *
     * @param float $amount
     * @return float
     */
    public static function appendNDS(float $amount): float
    {
        return round($amount * self::NDS_MULTIPLICATOR, 2);
    }

    public static function getNds(float $amount): float
    {
        return $amount - self::subtractNDS($amount);
    }
}