<?php

declare(strict_types = 1);

namespace App\Helper;

use App\Exception\InternalException;
use Generator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ResponseHelper
{

    /**
     * @param resource $stream
     * @param int      $code
     * @param array    $headers
     * @return Response
     * @throws InternalException
     */
    public static function stream($stream, int $code = 200, array $headers = []): Response
    {
        if (!is_resource($stream)) {
            throw new InternalException('Ожидается стрим');
        }

        return self::generator(Stream::generate($stream), $code, $headers);
    }

    /** @throws InternalException */
    public static function attachmentStream($stream, string $mimeType = 'application/octet-stream'): Response
    {
        return self::stream($stream, 200, ['Content-Type' => $mimeType, 'Content-Disposition' => 'attachment']);
    }

    /**
     * @param resource $stream
     * @param int      $code
     * @param array    $headers
     * @return Response
     */
    public static function generator(Generator $generator, int $code = 200, array $headers = []): Response
    {
        return new StreamedResponse(function () use ($generator) {
            foreach ($generator as $chunk) {
                echo $chunk;
            }
        }, $code, $headers);
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public static function jsonResponse(array $data): JsonResponse
    {
        $response = new JsonResponse();

        $response->setEncodingOptions(JsonResponse::DEFAULT_ENCODING_OPTIONS | JSON_PRESERVE_ZERO_FRACTION);
        $response->setData($data);

        return $response;
    }
}