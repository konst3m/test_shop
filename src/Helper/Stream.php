<?php

declare(strict_types = 1);

namespace App\Helper;

use App\Dictionary\Size;
use App\Exception\InternalException;
use Generator;

class Stream
{

    /**
     * @param resource $resource
     * @param int      $length
     * @return string
     * @throws InternalException
     */
    public static function fread($resource, int $length = Size::MB): string
    {
        if (!is_resource($resource)) {
            throw new InternalException('Ожидается стрим');
        }

        if (($chunk = fread($resource, $length)) === false) {
            throw new InternalException("Не удалось прочитать из стрима: {$length}");
        }

        return $chunk;
    }

    /**
     * @param resource $resource
     * @param int      $length
     * @return Generator
     * @throws InternalException
     */
    public static function generate($resource, int $length = Size::MB): Generator
    {
        while (!empty($chunk = self::fread($resource, $length))) {
            yield $chunk;
        }
    }

    /**
     * @param resource $resource
     * @param string   $chunk
     * @return void
     * @throws InternalException
     */
    public static function write($resource, string $chunk): void
    {
        if (fwrite($resource, $chunk) === false) {
            throw new InternalException('Не удалось записать в поток');
        }
    }
}