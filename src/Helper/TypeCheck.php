<?php

declare(strict_types = 1);

namespace App\Helper;

use App\Exception\Core\TypeCastException;
use App\Exception\Core\TypeException;

/**
 * Задача класса - проверка типа,
 * или кастовать в указанный тип если $strict = false
 */
class TypeCheck
{

    public const TYPE_FLOAT  = 'float';
    public const TYPE_ARRAY  = 'array';
    public const TYPE_INT    = 'int';
    public const TYPE_STRING = 'string';
    public const TYPE_BOOL   = 'bool';
    public const TYPE_OBJECT = 'object';

    public const     RETURN_TYPE_VOID      = 'void';
    public const     TYPES                 = [
        self::TYPE_FLOAT,
        self::TYPE_ARRAY,
        self::TYPE_INT,
        self::TYPE_STRING,
        self::TYPE_BOOL,
        self::TYPE_OBJECT,
    ];
    public const     TYPES_MAP             = [
        self::TYPE_BOOL   => self::TYPE_INTERNAL_BOOLEAN,
        self::TYPE_INT    => self::TYPE_INTERNAL_INTEGER,
        self::TYPE_FLOAT  => self::TYPE_INTERNAL_DOUBLE,
        self::TYPE_STRING => self::TYPE_INTERNAL_STRING,
        self::TYPE_ARRAY  => self::TYPE_INTERNAL_ARRAY,
        self::TYPE_OBJECT => self::TYPE_INTERNAL_OBJECT,
    ];
    private const    TYPE_INTERNAL_BOOLEAN = 'boolean';
    private const    TYPE_INTERNAL_INTEGER = 'integer';
    private const    TYPE_INTERNAL_DOUBLE  = 'double';
    private const    TYPE_INTERNAL_STRING  = 'string';
    private const    TYPE_INTERNAL_ARRAY   = 'array';
    private const    TYPE_INTERNAL_OBJECT  = 'object';
    private const    TYPE_INTERNAL_NULL    = 'null';

    public static function getInternalType($value): string
    {
        return strtolower(gettype($value));
    }

    /**
     * @param        $value
     * @param string $expectedType
     * @param bool   $strict
     * @return mixed
     * @throws TypeException
     */
    public static function cast($value, string $expectedType, bool $strict = false): mixed
    {
        $isClass = false;
        if (!in_array($expectedType, self::TYPES, true)) {
            if (class_exists($expectedType)) {
                $isClass = true;
            } else {
                throw new TypeException("Не валидный тип $expectedType");
            }
        }
        $currentType = self::getInternalType($value);

        if (
            !$strict && self::TYPE_INTERNAL_NULL === $currentType
            && in_array($expectedType, [self::TYPE_INT, self::TYPE_FLOAT, self::TYPE_STRING], true)
        ) {
            if ($expectedType === self::TYPE_INT) {
                return 0;
            }

            if ($expectedType === self::TYPE_FLOAT) {
                return 0.0;
            }

            return '';
        }

        if ($isClass) {
            if ($currentType !== self::TYPE_INTERNAL_OBJECT) {
                if ($strict) {
                    throw new TypeCastException($expectedType, $currentType);
                }

                return $value;
            }
            if (!$value instanceof $expectedType && $strict) {
                throw new TypeCastException($expectedType, $value::class);
            }

            return $value;
        }

        // base
        if (isset(self::TYPES_MAP[$expectedType]) && self::TYPES_MAP[$expectedType] === $currentType) {
            return $value;
        }

        // object -> string
        if (
            $expectedType === self::TYPE_STRING
        ) {
            // object::__toString
            if ($currentType === self::TYPE_INTERNAL_OBJECT && method_exists($value, '__toString')) {
                return (string) $value;
            }

            // int, float
            if (in_array($currentType, [self::TYPE_INTERNAL_INTEGER, self::TYPE_INTERNAL_DOUBLE], true)) {
                return (string) $value;
            }
        }

        // string -> float, int
        if (
            $currentType === self::TYPE_INTERNAL_STRING
            && ($expectedType === self::TYPE_FLOAT || $expectedType === self::TYPE_INT)
            && !$strict
        ) {
            $castedValue = $expectedType === self::TYPE_FLOAT ? (float) $value : (int) $value;
            if ((string) $castedValue === $value) {
                return $castedValue;
            }

            // Если на конце есть 0
            if (is_numeric($value) && $value[strlen($value) - 1] === '0') {
                return $castedValue;
            }

            if (is_numeric($value)) {
                return (float) $value;
            }

            throw new TypeException("Попытка кастовать значение '$value' в $expectedType");
        }

        // int -> float
        if ($currentType === self::TYPE_INTERNAL_INTEGER && $expectedType === self::TYPE_FLOAT && !$strict) {
            return (float) $value;
        }

        // float -> int
        if (
            $currentType === self::TYPE_INTERNAL_DOUBLE
            && $expectedType === self::TYPE_INT
            && !$strict
            && $value === (float) (int) $value
        ) {
            return (int) $value;
        }

        throw new TypeCastException($expectedType, $currentType);
    }

    /** @throws TypeException */
    public static function int($value, bool $strict = false): int
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return self::cast($value, self::TYPE_INT, $strict);
    }

    public static function float($value): float
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return self::cast($value, self::TYPE_FLOAT);
    }

    /** @throws TypeException */
    public static function string($value): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return self::cast($value, self::TYPE_STRING);
    }

    /**
     * @param        $value
     * @param string $type
     * @param bool   $strict
     * @return mixed
     * @throws TypeCastException
     * @throws TypeException
     */
    public static function returnType($value, string $type, bool $strict = false): mixed
    {
        [$expectedType, $isNullable] = self::parseReturnType($type);
        $currentType = self::getInternalType($value);
        if ($expectedType === self::RETURN_TYPE_VOID) {
            if ($isNullable) {
                throw new TypeException("Невалидный тип $type");
            }

            if ($currentType === self::TYPE_INTERNAL_NULL) {
                return null;
            }

            throw new TypeCastException($type, $currentType);
        }

        if ($isNullable && $currentType === self::TYPE_INTERNAL_NULL) {
            return null;
        }

        return self::cast($value, $expectedType, $strict);
    }

    private static function parseReturnType(string $type): array
    {
        if ($type[0] === '?') {
            return [substr($type, 1), true];
        }

        return [$type, false];
    }
}