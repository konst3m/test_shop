<?php

declare(strict_types = 1);

namespace App\Http\Dto;

class OrderReportDTO
{

    public function __construct(
        public readonly string $product_name,
        public readonly float $price,
        public readonly UserOrderReportDTO $user,
        public readonly ?int $amount = 1,
    ) {
    }
}