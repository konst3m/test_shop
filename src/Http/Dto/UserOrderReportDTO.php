<?php

declare(strict_types = 1);

namespace App\Http\Dto;

class UserOrderReportDTO
{

    public function __construct(
        public readonly int $id,
    ) {
    }
}