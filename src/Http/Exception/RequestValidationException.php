<?php

declare(strict_types = 1);

namespace App\Http\Exception;

use App\Exception\InternalException;
use App\Exception\PublicError;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Throwable;

/**
 * Исключения, возникшие во время валидации http запроса
 */
class RequestValidationException extends InternalException implements PublicError
{

    public function __construct(
        string $message,
        int $code = 0,
        Throwable $previous = null,
        public readonly ?ConstraintViolationInterface $error = null,
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getPublicMessage(): string
    {
        return $this->getMessage();
    }

    public function getPublicData(): array
    {
        return [];
    }
}