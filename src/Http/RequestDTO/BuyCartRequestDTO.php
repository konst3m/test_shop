<?php

declare(strict_types = 1);

namespace App\Http\RequestDTO;

use App\Entity\Enum\DeliveryType;
use App\Entity\User;

class BuyCartRequestDTO
{

    public function __construct(
        public readonly int $id,
        public readonly string $phone,
        public readonly DeliveryType $deliveryType,
        public readonly User $user,
    ) {
    }
}