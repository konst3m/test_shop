<?php

declare(strict_types = 1);

namespace App\Http\RequestDTO;

use App\Entity\Product;
use App\Entity\User;

class CartRequestDTO
{

    public function __construct(
        readonly public Product $product,
        readonly public User $user,
    ) {
    }
}