<?php
declare(strict_types = 1);

namespace App\Http\RequestDTO;

class Measurements
{

    public function __construct(
        readonly public float $weight,
        readonly public float $height,
        readonly public float $width,
        readonly public float $length,
    )
    {
    }
}