<?php

declare(strict_types = 1);

namespace App\Http\RequestDTO;

class ProductRequestDTO
{

    public function __construct(
        readonly public string $name,
        readonly public string $description,
        readonly public float $cost,
        readonly public float $tax,
        readonly public int $version,
        readonly public Measurements $measurements,
        readonly public ?int $id = null,
    ) {
    }
}