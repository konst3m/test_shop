<?php

declare(strict_types = 1);

namespace App\Http\RequestDTOExtractor;

use App\Entity\User;
use App\Exception\InternalException;
use App\Exception\NullException;
use App\Http\Exception\RequestValidationException;
use App\System\Validation\SequenceCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractExtractor
{

    /** @var array<mixed> Кешированные данные полей */
    protected array $fieldsData = [];

    /** @var bool Производить ли инициализацию при создании */
    protected bool $init = true;

    /** @var bool Признак, что данные проинициализированы */
    protected bool $initialized = false;

    /** @var string[] Массив ключей, данных по которым должны прийти в этом запросе */
    protected array $fieldsNames = [];

    /** @var string[]|callable[] Дефолтные значения полей */
    protected array $defaultFieldsValues = [];

    /** @var callable[] Колбеки для трансформации полей */
    protected array $transformers = [];

    /** @var bool Проверять ли аутентификацию */
    protected bool $checkAuth = true;

    protected ?Request $request;

    private EntityManagerInterface $entityManager;
    private ValidatorInterface $validator;
    private RequestStack $requestStack;

    /** @var callable[] Кастомные геттеры полей */
    private array $customFieldsGetters = [];

    /** @var callable[] Геттеры зависимых полей */
    private array $dependedFieldsGetters = [];

    /**
     * Возвращает DTO запроса
     *
     * @return mixed
     *
     * @throw RequestValidationException
     */
    abstract public function extract(): mixed;

    /**
     * @throws NullException
     * @throws InternalException
     *
     * @noinspection PhpUnused
     */
    #[Required]
    public function localInitializer(
        RequestStack $requestStack,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
    ): void {
        $this->requestStack = $requestStack;
        $this->validator = $validator;
        $this->entityManager = $entityManager;

        if ($this->shouldInitOnStart()) {
            $this->init();
        }
    }

    /**
     * @throws InternalException
     * @throws NullException
     */
    public function init(): void
    {
        if ($this->initialized) {
            throw new InternalException('Экстрактор уже инициализирован');
        }

        $this->request = $this->requestStack->getCurrentRequest();

        if ($this->request === null) {
            throw new NullException();
        }

        if ($this->request !== $this->requestStack->getMainRequest()) {
            throw new InternalException(
                'Экстрактор можно запускать только на корневом запросе',
            );
        }

        $this->fieldsData = $this->getFieldsData();

//        if ($this->checkAuth) {
//            $this->getUser();
//        }

        $this->initialized = true;
    }

    protected function shouldInitOnStart(): bool
    {
        return $this->init;
    }

    /** @return string[]|callable[] */
    protected function getDefaultFieldValues(): array
    {
        return $this->defaultFieldsValues;
    }

    /**
     * Геттер значения для указанного поля
     */
    protected function getFieldValue(string $field): mixed
    {
        $value = null;
        if (isset($this->customFieldsGetters[$field])) {
            $value = call_user_func($this->customFieldsGetters[$field], $this->request);
        } elseif (!isset($this->dependedFieldsGetters[$field])) {
            $value = $this->defaultFieldGetter($field);
        }

        if ($value === null) {
            $defaultFieldsValues = $this->getDefaultFieldValues();
            if (isset($defaultFieldsValues[$field])) {
                if (is_callable($defaultFieldsValues[$field])) {
                    $value = call_user_func($defaultFieldsValues[$field]);
                } else {
                    $value = $defaultFieldsValues[$field];
                }
            }
        }

        return $value;
    }

    /**
     * Специфические геттеры для полей
     * Нужно переопределять в дочерних классах
     * Это не аналог $defaultFieldsValues, т.к. тут геттер может вернуть null,
     * и после этого будет выборка из $defaultFieldsValues
     *
     * @return callable[]
     */
    protected function getFieldsGetters(): array
    {
        return [];
    }

    /**
     * Возвращает геттеры зависимых полей
     *
     * @return callable[]
     */
    protected function getDependedFieldsGetters(): array
    {
        return [];
    }

    /**
     * Возвращает правила для валидации запроса
     *
     * @return Collection|SequenceCollection
     */
    protected function getRules(): Collection|SequenceCollection
    {
        return new SequenceCollection([]);
    }

    /**
     * Возвращает авторизованного юзера
     *
     * @return User
     */
//    protected function getUser(): User
//    {
//    }

    /**
     * Валидирует запрошенные данные
     */
    protected function validateFieldsData(): ConstraintViolationListInterface
    {
        $rules = $this->getRules();
        $rules->allowExtraFields = true;

        return $this->validator->validate($this->fieldsData, $rules);
    }

    /**
     * Возвращает entity manager
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * Возвращает данные из запроса по ключам из
     *
     * @see AbstractExtractor::getFieldNames()
     *
     * @return array<mixed>
     */
    protected function getFieldsData(): array
    {
        $this->customFieldsGetters = $this->getFieldsGetters();
        $this->dependedFieldsGetters = $this->getDependedFieldsGetters();

        foreach ($this->getFieldNames() as $field) {
            $this->fieldsData[$field] = $this->getFieldValue($field);
        }

        foreach ($this->getFieldNames() as $field) {
            if (isset($this->dependedFieldsGetters[$field])) {
                $this->fieldsData[$field] = call_user_func(
                    $this->dependedFieldsGetters[$field],
                    $this->fieldsData,
                    $this->request,
                );
            }
        }

        return $this->fieldsData;
    }

    /**
     * Возвращает трансформированные значения полей
     *
     * @param array $data
     * @return array<string, mixed>
     */
    protected function transformFieldsData(array $data): array
    {
        foreach ($data as $key => $value) {
            if (isset($this->transformers[$key])) {
                $data[$key] = call_user_func($this->transformers[$key], $value);
            } else {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    /**
     * Возвращает валидный массив данных или бросает исключение
     *
     * @return array<mixed>
     * @throws InternalException
     * @throws RequestValidationException
     */
    protected function getValidatedFieldsData(): array
    {
        if (!$this->initialized) {
            throw new InternalException('Экстрактор не инициализирован');
        }

        $errors = $this->validateFieldsData();
        if ($errors->count()) {
            throw new RequestValidationException(
                (string) $errors->get(0)->getMessage(),
                error: $errors->get(0),
            );
        }

        return $this->transformFieldsData($this->fieldsData);
    }

    /**
     * Стандартный геттер для всех значений всех полей
     */
    protected function defaultFieldGetter(string $field): mixed
    {
        return $this->request->get($field) ?? $this->request->files->get($field);
    }

    /** @return string[] */
    protected function getFieldNames(): array
    {
        return $this->fieldsNames;
    }
}