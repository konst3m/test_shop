<?php
declare(strict_types = 1);

namespace App\Http\RequestDTOExtractor;

use App\Entity\Enum\DeliveryType;
use App\Entity\User;
use App\Exception\InternalException;
use App\Helper\TypeCheck;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTO\BuyCartRequestDTO;
use App\Service\CartService;
use App\Service\ShopOrderService;
use App\Service\UserService;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class BuyCartDTOExtractor extends AbstractExtractor
{

    private User|null $user = null;

    public function __construct(
        private readonly CartService $cartService,
        private readonly ShopOrderService $orderService,
        private readonly UserService $userService,
    ) {
    }

    protected array $fieldsNames = [
        'id',
        'phone',
        'delivery_type',
        'user_id',
    ];

    /**
     * @throws RequestValidationException
     * @throws InternalException
     */
    public function extract(): BuyCartRequestDTO
    {
        $data = $this->getValidatedFieldsData();

        if ($this->user === null) {
            throw new InternalException('пользователь не найден');
        }

        return new BuyCartRequestDTO(
            TypeCheck::int($data['id']),
            $data['phone'],
            DeliveryType::from($data['delivery_type']),
            $this->user
        );
    }

    protected function getRules(): Collection
    {
        return new Collection([
            'user_id'       => [
                new NotBlank(['message' => 'Параметр обязателен']),
                new Callback([
                    'callback' => function ($id, $context) {
                        $user = $this->userService->getUser(TypeCheck::int($id));
                        $this->user = $user;
                    },
                ]),
            ],
            'id'            => [
                new NotBlank(['message' => 'Параметр обязателен']),
                new Callback([
                    'callback' => function (string $id, ExecutionContext $context) {
                        $cart = $this->cartService->getOrCreateCart($this->user);
                        $order = $this->orderService->getActualShopOrderByCart($cart);
                        if (count($order->products) === 0) {
                            throw new InternalException('должен быть добавлен хотя-бы 1 товар');
                        }
                    },
                ]),
            ],
            'phone'         => [
                new NotBlank(['message' => 'Телефон должен быть указан']),
                new Length([
                    'min'        => 4,
                    'max'        => 30,
                    'minMessage' => 'Слишком короткий телефон',
                    'maxMessage' => 'Телефон слишком длинный',
                ]),
                new Callback([
                    'callback' => function (string $value, ExecutionContextInterface $context) {
                        if ($value[0] !== '+') {
                            $context->addViolation('Телефон должен начинаться с +');
                        }

                        $value = ltrim($value, '+');
                        if (preg_match('/\D/', $value) !== 0) {
                            $context->addViolation('В телефоне должны быть только + и цифры');
                        }
                    },
                ]),
            ],
            'delivery_type' => [
                new NotBlank(['message' => 'Параметр обязателен']),
            ],
        ]);
    }
}