<?php
declare(strict_types = 1);

namespace App\Http\RequestDTOExtractor;

use App\Entity\Product;
use App\Entity\User;
use App\Exception\InternalException;
use App\Helper\TypeCheck;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTO\CartRequestDTO;
use App\Service\ProductService;
use App\Service\UserService;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use \Symfony\Component\Validator\Constraints\Callback;

class CartDTOExtractor extends AbstractExtractor
{

    private Product|null $product = null;
    private User|null $user = null;

    public function __construct(
        private readonly ProductService $productService,
        private readonly UserService $userService,
    ) {
    }

    protected array $fieldsNames = [
        'product_id',
        'user_id',
    ];

    /**
     * @throws RequestValidationException
     * @throws InternalException
     */
    public function extract(): CartRequestDTO
    {
        $data = $this->getValidatedFieldsData();

        if ($this->product === null) {
            throw new InternalException('товар не найден');
        }

        if ($this->user === null) {
            throw new InternalException('пользователь не найден');
        }

        return new CartRequestDTO($this->product, $this->user);
    }

    protected function getRules(): Collection
    {
        return new Collection([
            'product_id' => [
                new NotBlank(['message' => 'Параметр обязателен']),
                new Callback([
                    'callback' => function ($id, $context) {
                        $product = $this->productService->getProductById(TypeCheck::int($id));

                        if ($product === null) {
                            $context->addViolation('Товар не найден');

                            return;
                        }

                        $this->product = $product;
                    },
                ]),
            ],
            'user_id' => [
                new NotBlank(['message' => 'Параметр обязателен']),
                new Callback([
                    'callback' => function ($id, $context) {
                        $user = $this->userService->getUser(TypeCheck::int($id));
                        $this->user = $user;
                    },
                ]),
            ]
        ]);
    }
}