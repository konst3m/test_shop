<?php
declare(strict_types = 1);

namespace App\Http\RequestDTOExtractor;

use App\Exception\InternalException;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTO\CreateOrderRequestDTO;
use App\System\Validation\Period;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderDTOExtractor extends AbstractExtractor
{
    protected array $fieldsNames = [
        'id',
    ];

    /**
     * @throws RequestValidationException
     * @throws InternalException
     */
    public function extract(): CreateOrderRequestDTO
    {
        $data = $this->getValidatedFieldsData();

        return new CreateOrderRequestDTO($data['id']);
    }


    protected function getRules(): Collection
    {
        return new Collection([
            'user_id' => [

            ],
            'period'  => [

            ],
        ]);
    }
}