<?php
declare(strict_types = 1);

namespace App\Http\RequestDTOExtractor;

use App\Exception\InternalException;
use App\Helper\TypeCheck;
use App\Http\Exception\RequestValidationException;
use App\Http\RequestDTO\Measurements;
use App\Http\RequestDTO\ProductRequestDTO;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Type;

class ProductDTOExtractor extends AbstractExtractor
{

    protected array $fieldsNames = [
        'id',
        'name',
        'description',
        'cost',
        'tax',
        'version',
        'measurements',
    ];

    /**
     * @throws RequestValidationException
     * @throws InternalException
     */
    public function extract(): ProductRequestDTO
    {
        $data = $this->getValidatedFieldsData();

        return new ProductRequestDTO(
            TypeCheck::string($data['name']),
            TypeCheck::string($data['description']),
            TypeCheck::float($data['cost']),
            TypeCheck::float($data['tax']),
            TypeCheck::int($data['version']),
            new Measurements(
                TypeCheck::float($data['measurements']['weight']),
                TypeCheck::float($data['measurements']['height']),
                TypeCheck::float($data['measurements']['width']),
                TypeCheck::float($data['measurements']['length']),
            ),
            TypeCheck::int($data['id']),
        );
    }

    protected function getRules(): Collection
    {
        return new Collection([
            'name'         => [
                new NotBlank(['message' => 'Укажите имя']),
            ],
            'description'  => [
                new NotBlank(['message' => 'Укажите описание']),
            ],

            'cost'         => [
                new NotBlank(['message' => 'Укажите цену']),
                new Positive(),
            ],

            'tax'          => [
                new NotBlank(['message' => 'Укажите налог']),
                new Positive(),
            ],

            'version'      => [
                new NotBlank(['message' => 'Укажите версию']),
                new Positive(),
            ],

            'measurements' => [
                new Type('array'),
                new Collection([
                    'weight' => [
                        new NotBlank(),
                        new Positive(),
                    ],
                    'height' => [
                        new NotBlank(),
                        new Positive(),
                    ],
                    'width'  => [
                        new NotBlank(),
                        new Positive(),
                    ],
                    'length'  => [
                        new NotBlank(),
                        new Positive(),
                    ],
                ]),
            ],
        ]);
    }
}