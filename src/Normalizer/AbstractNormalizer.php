<?php

declare(strict_types = 1);

namespace App\Normalizer;

use App\Exception\InternalException;
use App\System\Doctrine\WithPreloader;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class AbstractNormalizer implements NormalizerInterface, NormalizerAwareInterface
{

    protected static bool $preloaderExecuted = false;
    protected static bool $isMaxQueryCountSet = false;

    protected NormalizerInterface $serializer;

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     * @return mixed
     * @throws InternalException
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        if (!$this instanceof WithPreloader) {
            throw new InternalException('Не определен нормалайзер для: ' . $this::class);
        }

        if (!static::$isMaxQueryCountSet && isset($context['__maxQueryCountSet'])) {
            call_user_func($context['__maxQueryCountSet'], $this->getMaxQueryWhileNormalize());

            unset($context['__maxQueryCountSet']);
            static::$isMaxQueryCountSet = true;
        }

        if (!static::$preloaderExecuted && isset($context['__entityPreloader'])) {
            call_user_func($context['__entityPreloader'], $this->getPreloadPaths($context));

            unset($context['__entityPreloader']);
            static::$preloaderExecuted = true;
        }

        return $this->normalizeWithPreloader($object, $format, $context);
    }

    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->serializer = $normalizer;
    }

    protected function getMaxQueryWhileNormalize(): int
    {
        return PHP_INT_MAX;
    }

    protected function hasGroup(array $context, string $group): bool
    {
        return in_array($group, $context['groups'] ?? [], true);
    }
}