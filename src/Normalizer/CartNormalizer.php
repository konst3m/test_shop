<?php

declare(strict_types = 1);

namespace App\Normalizer;

use App\Entity\Cart;
use App\Service\ShopOrderService;
use Throwable;

class CartNormalizer extends AbstractNormalizer
{
    public function __construct(
        private readonly ShopOrderService $orderService,
    ) { }

    /**
     * @param Cart $object
     * @throws Throwable
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        $order = $this->orderService->getActualShopOrderByCart($object);

        $data = [
            'id'           => $object->id,
            'products'     => $order->products,
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Cart;
    }

}