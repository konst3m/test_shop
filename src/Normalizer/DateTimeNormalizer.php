<?php

declare(strict_types = 1);

namespace App\Normalizer;

use DateTimeInterface;
use Throwable;

class DateTimeNormalizer extends AbstractNormalizer
{

    /**
     * @param DateTimeInterface $object
     * @throws Throwable
     */
    public function normalize($object, string $format = null, array $context = []): int
    {
        return $object->getTimestamp();
    }

    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof DateTimeInterface;
    }

    public function getSupportedTypes(?string $format): array
    {
        // Возвращаем массив типов, которые поддерживаются нормализатором
        return [DateTimeInterface::class => true];
    }
}