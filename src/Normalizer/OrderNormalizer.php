<?php

declare(strict_types = 1);

namespace App\Normalizer;

use App\Entity\ShopOrder;
use Throwable;

class OrderNormalizer extends AbstractNormalizer
{

    /**
     * @param ShopOrder $object
     * @throws Throwable
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        $data = [
            'id'           => $object->id,
            'products'     => $object->products,
            'deliveryType' => $object->deliveryType,
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof ShopOrder;
    }

}