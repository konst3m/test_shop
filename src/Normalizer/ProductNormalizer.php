<?php

declare(strict_types = 1);

namespace App\Normalizer;

use App\Entity\Product;
use Throwable;

class ProductNormalizer extends AbstractNormalizer
{

    /**
     * @param Product $object
     * @throws Throwable
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        $data = [
            'id'          => $object->getId(),
            'name'        => $object->name,
            'description' => $object->description,
            'height'      => $object->height,
            'weight'      => $object->weight,
            'length'      => $object->length,
            'width'       => $object->width,
            'version'     => $object->version,
            'tax'         => $object->tax,
            'coast'       => $object->getAmount()->getWithVAT(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Product;
    }

}