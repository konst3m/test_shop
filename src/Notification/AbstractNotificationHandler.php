<?php
declare(strict_types = 1);

namespace App\Notification;

use App\Event\OrderNotificationEvent;

abstract class AbstractNotificationHandler
{
    abstract public function execute(OrderNotificationEvent $event): bool;

    abstract public function isHandled(OrderNotificationEvent $event): bool;

}