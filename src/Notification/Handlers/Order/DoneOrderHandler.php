<?php
declare(strict_types = 1);

namespace App\Notification\Handlers\Order;

use App\Entity\Enum\OrderStatus;
use App\Event\OrderNotificationEvent;

class DoneOrderHandler extends AbstractOrderHandler
{

    private const DONE = OrderStatus::DONE;

    public function execute(OrderNotificationEvent $event): bool
    {
        return true;
    }

    public function isHandled(OrderNotificationEvent $event): bool
    {
        if (self::DONE === $event->shopOrder->orderStatus) {
            return true;
        }

        return false;
    }
}