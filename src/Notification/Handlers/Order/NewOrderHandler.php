<?php
declare(strict_types = 1);

namespace App\Notification\Handlers\Order;

use App\Entity\Enum\OrderStatus;
use App\Event\OrderNotificationEvent;

class NewOrderHandler extends AbstractOrderHandler
{

    private const NEW = OrderStatus::NEW;

    public function execute(OrderNotificationEvent $event): bool
    {
        return true;
    }

    public function isHandled(OrderNotificationEvent $event): bool
    {
        if (self::NEW === $event->shopOrder->orderStatus) {
            return true;
        }

        return false;
    }
}