<?php
declare(strict_types = 1);

namespace App\Notification\Handlers\Order;

use App\Entity\Enum\OrderStatus;
use App\Event\OrderNotificationEvent;

class PayedOrderHandler extends AbstractOrderHandler
{

    private const PAID = OrderStatus::PAID;

    public function execute(OrderNotificationEvent $event): bool
    {
        // отправка сообщения
        return true;
    }

    public function isHandled(OrderNotificationEvent $event): bool
    {
        if (self::PAID === $event->shopOrder->orderStatus) {
            return true;
        }

        return false;
    }
}