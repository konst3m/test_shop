<?php
declare(strict_types = 1);

namespace App\Notification;

use App\Event\OrderNotificationEvent;
use App\Notification\Handlers\Order\AbstractOrderHandler;

class NotificationProcessor
{

    /** @param AbstractOrderHandler[] $handlers */
    public function __construct(
        private readonly array $handlers,
    ) {
    }

    public function sendNotification(OrderNotificationEvent $event): void
    {
        foreach ($this->handlers as $handler) {
            if($handler->isHandled($event)) {
                $handler->execute($event);
            }
        }
    }
}