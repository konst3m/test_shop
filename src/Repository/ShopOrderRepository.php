<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\Enum\OrderStatus;
use App\Entity\ShopOrder;
use App\Exception\InternalException;
use Carbon\CarbonImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ShopOrder>
 *
 * @method ShopOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopOrder[]    findAll()
 * @method ShopOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopOrderRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopOrder::class);
    }

    /**
     * @param Cart $cart
     * @return ShopOrder|null
     * @throws InternalException
     */
    public function getActualShopOrderByCart(Cart $cart): ShopOrder
    {
        try {
            $qb = $this->createQueryBuilder('so')
                ->where('so.orderStatus = :status')
                ->andWhere('so.cart = :cart')
                ->andWhere('so.user = :user')
                ->setParameter('status', OrderStatus::NEW)
                ->setParameter('cart', $cart)
                ->setParameter('user', $cart->user);

            return $qb->getQuery()->getSingleResult();
        } catch (NonUniqueResultException $e) {
            throw new NonUniqueResultException('Найдено несколько результатов');
        } catch (NoResultException $e) {
            throw new InternalException('На корзине всегда должен быть ShopOrder');
        }
    }

    /** @return ShopOrder[] */
    public function getOrdersForReport(): array
    {
        $qb = $this->createQueryBuilder('so')
            ->where('so.orderStatus = :status')
            ->andWhere('so.reportNumber IS NULL')
            ->setParameter('status', OrderStatus::DONE);

        return $qb->getQuery()->getResult();
    }
}
