<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Enum\OrderStatus;
use App\Entity\User;
use App\Exception\InternalException;
use App\Http\RequestDTO\BuyCartRequestDTO;
use App\Http\RequestDTO\CartRequestDTO;
use App\Repository\CartRepository;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class CartService
{

    public function __construct(
        private readonly ShopOrderService $orderService,
        private readonly CartRepository $cartRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @throws InternalException
     */
    public function addToCart(CartRequestDTO $dto): Cart
    {
        $cart = $this->getOrCreateCart($dto->user);
        $actualOrder = $this->orderService->getActualShopOrderByCart($cart);

        if (!$actualOrder->products->contains($dto->product)) {
            $actualOrder->addProduct($dto->product);
            $this->entityManager->persist($actualOrder);
            $this->entityManager->flush();
        }

        return $cart;
    }

    /**
     * @throws InternalException
     */
    public function removeFromCart(CartRequestDTO $dto): Cart
    {
        $cart = $this->getOrCreateCart($dto->user);
        $actualOrder = $this->orderService->getActualShopOrderByCart($cart);

        if ($actualOrder->products->contains($dto->product)) {
            $actualOrder->removeProduct($dto->product);
            $this->entityManager->persist($actualOrder);
            $this->entityManager->flush();
        }

        return $cart;
    }

    /**
     * @throws InternalException
     */
    public function buyCart(BuyCartRequestDTO $dto): Cart
    {
        $cart = $this->getOrCreateCart($dto->user);
        $actualOrder = $this->orderService->getActualShopOrderByCart($cart);
        $actualOrder->phone = $dto->phone;
        $actualOrder->deliveryType = $dto->deliveryType;
        $actualOrder->orderStatus = OrderStatus::PAID;
        $actualOrder->setUpdatedAt(CarbonImmutable::now());

        $order = $this->orderService->createOrder($dto->user);
        $cart->addShopOrder($order);

        $this->entityManager->persist($order);
        $this->entityManager->persist($cart);
        $this->entityManager->persist($actualOrder);
        $this->entityManager->flush();

        return $cart;
    }

    /**
     * @throws InternalException
     */
    public function dropCart(User $user): Cart
    {
        $cart = $this->getOrCreateCart($user);
        $actualOrder = $this->orderService->getActualShopOrderByCart($cart);
        $actualOrder->products = new ArrayCollection();
        $this->entityManager->persist($actualOrder);
        $this->entityManager->flush();

        return $cart;
    }

    public function getOrCreateCart(User $user): Cart
    {
        $cart = $this->cartRepository->findOneBy(['user' => $user]);

        if ($cart === null) {
            $cart = new Cart($user);
            $order = $this->orderService->createOrder($user);
            $cart->addShopOrder($order);
            $this->entityManager->persist($order);
            $this->entityManager->persist($cart);
            $this->entityManager->flush();
        }

        return $cart;
    }

}