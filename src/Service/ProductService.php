<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\Product;
use App\Exception\InternalException;
use App\Http\RequestDTO\ProductRequestDTO;
use App\Repository\ProductRepository;
use App\Value\Amount\Amount;
use App\Value\CurrencyCode;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Carbon\CarbonImmutable;
use Doctrine\ORM\EntityManagerInterface;

class ProductService
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ProductRepository $productRepository,
    ) {
    }

    public function getProductById(int $id): Product|null
    {
        return $this->productRepository->findOneBy(['id' => $id]);
    }

    public function getList(): array
    {
        return $this->productRepository->findAll();
    }

    /**
     * @throws RoundingNecessaryException
     * @throws DivisionByZeroException
     * @throws InternalException
     * @throws NumberFormatException
     */
    public function createProduct(ProductRequestDTO $dto): Product
    {
        return $this->makeProduct($dto, new Product());
    }

    /**
     * @throws DivisionByZeroException
     * @throws RoundingNecessaryException
     * @throws InternalException
     * @throws NumberFormatException
     */
    public function updateProduct(ProductRequestDTO $dto): Product
    {
        if ($dto->id === null) {
            throw new InternalException('id продукта не может быть null');
        }

        $product = $this->productRepository->findOneBy(['id' => $dto->id]);

        if ($product === null) {
            throw new InternalException('продукт не найден id: ' . $dto->id);
        }

        return $this->makeProduct($dto, $product);
    }

    /**
     * @throws RoundingNecessaryException
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     * @throws InternalException
     */
    private function makeProduct(ProductRequestDTO $dto, Product $product): Product
    {
        $amount = Amount::makeByWithVAT($dto->cost, CurrencyCode::RUB);
        $product->name = $dto->name;
        $product->description = $dto->description;
        $product->height = $dto->measurements->height;
        $product->weight = $dto->measurements->weight;
        $product->length = $dto->measurements->length;
        $product->width = $dto->measurements->width;
        $product->version = $dto->version;
        $product->tax = $dto->tax;
        $product->setAmount($amount);
        $product->setCreatedAt(CarbonImmutable::now());
        $product->setUpdatedAt(CarbonImmutable::now());

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

}