<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Enum\OrderStatus;
use App\Entity\Product;
use App\Entity\ShopOrder;
use App\Entity\User;
use App\Exception\InternalException;
use App\Http\Dto\OrderReportDTO;
use App\Http\Dto\UserOrderReportDTO;
use App\Repository\ShopOrderRepository;
use App\Traits\UtilityTrait;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Carbon\CarbonImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ReportOrderService
{

    use UtilityTrait;

    public function __construct(
        private readonly ShopOrderRepository $orderRepository,
        private readonly NormalizerInterface $normalizer,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }



    /**
     * @throws \JsonException
     * @throws InternalException
     * @throws Exception
     */
    public function generateReport(string $path): string {
        $reportName = self::generateReportNumber();

        $this->entityManager->wrapInTransaction(
            function () use ($path, $reportName) {
                try {
                    $orders = $this->createReport($reportName);
                } catch (\Throwable $e) {
                    throw new InternalException($e->getMessage());
                }

                $test = $this->normalizer->normalize(json_encode($orders, JSON_THROW_ON_ERROR));

                $filePath = $path . '/reports/'.$reportName.'.json';
                file_put_contents($filePath, $this->normalizer->normalize($test));
                $this->entityManager->flush();
            }
        );

        return $reportName;
    }


    /**
     * @return OrderReportDTO[]
     * @throws RoundingNecessaryException
     * @throws NumberFormatException|DivisionByZeroException
     */
    private function createReport(string $reportNumber): array
    {
        $reportElements = [];

        $shopOrders = $this->getOrdersForReport();

        foreach ($shopOrders as $order) {
            /** @var Product $product */
            foreach ($order->products as $product) {
                $reportItem = new OrderReportDTO(
                    product_name: $product->name,
                    price: $product->getAmount()->getWithVAT(),
                    user: new UserOrderReportDTO($order->user->getId())
                );

                $reportElements[] = $reportItem;
            }

            $order->reporterAt = CarbonImmutable::now();
            $order->reportNumber = $reportNumber;
            $this->entityManager->persist($order);
        }

        return $reportElements;
    }

    /** @return ShopOrder[] */
    private function getOrdersForReport(): array
    {
        return $this->orderRepository->getOrdersForReport();
    }

}