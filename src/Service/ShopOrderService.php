<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Enum\OrderStatus;
use App\Entity\ShopOrder;
use App\Entity\User;
use App\Exception\InternalException;
use App\Repository\ShopOrderRepository;
use App\Traits\UtilityTrait;
use Carbon\CarbonImmutable;

class ShopOrderService
{

    use UtilityTrait;

    public function __construct(
        private readonly ShopOrderRepository $orderRepository,
    ) {
    }

    /**
     * @param User $user
     * @return ShopOrder
     */
    public function createOrder(User $user): ShopOrder
    {
        $order = new ShopOrder($user);
        $order->orderStatus = OrderStatus::NEW;
        $order->setUpdatedAt(CarbonImmutable::now());
        $order->setCreatedAt(CarbonImmutable::now());

        return $order;
    }

    /**
     * @throws InternalException
     */
    public function getActualShopOrderByCart(Cart $cart): ShopOrder
    {
        return $this->orderRepository->getActualShopOrderByCart($cart);
    }

}