<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;

class UserService
{

    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    // Заглушка для получения юзера по id или просто первого.
    public function getUser(?int $id = null): User
    {
        if ($id === null) {
            return $this->userRepository->findAll()[0];
        }

        return $this->userRepository->findOneBy(['id' => $id]);
    }
}