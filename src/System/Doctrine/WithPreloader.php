<?php

declare(strict_types = 1);

namespace App\System\Doctrine;

interface WithPreloader
{

    public function normalizeWithPreloader(mixed $object, string $format = null, array $context = []): mixed;

    public function getPreloadPaths(array $context = []): array;
}
