<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

abstract class AbstractValidator extends ConstraintValidator
{

    abstract public function run(mixed $value, Constraint $constraint): mixed;

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!isset($value)) {
            return;
        }

        $resultValue = $this->run($value, $constraint);

        if ($constraint instanceof TapableConstraint && $this->context->getViolations()->count() === 0) {
            $this->tap($constraint, $resultValue);
        }
    }

    private function tap(TapableConstraint $constraint, mixed $value): void
    {
        if ($tap = $constraint->getTap()) {
            $tap($value);
        }
    }
}