<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;

/** @see AmountValidator */
class Amount extends Constraint implements TapableConstraint
{

    public const MODE_POSITIVE = 0 ** 2;

    public function __construct(
        public readonly int $mode = 0,
        public readonly ?array $currencies = null,
        public readonly mixed $tap = null,
        public readonly mixed $validate = null,
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }

    public function getTap(): ?callable
    {
        return $this->tap;
    }
}