<?php

declare(strict_types = 1);

namespace App\System\Validation;

use App\Exception\InternalException;
use App\Helper\TypeCheck;
use App\Value\CurrencyCode;
use Symfony\Component\Validator\Constraint;

class AmountValidator extends AbstractValidator
{

    /**
     * @param mixed  $allConditions
     * @param Amount $constraint
     * @return \App\Value\Amount\Amount|null
     * @throws InternalException
     */
    public function run(mixed $value, Constraint $constraint): ?\App\Value\Amount\Amount
    {
        if (!isset($value['amount'], $value['currency'])) {
            $this->context->addViolation('Объем передан не корректно');

            return null;
        }

        $currencies = CurrencyCode::ALL;
        if (isset($constraint->currencies)) {
            if (count(array_filter($constraint->currencies, fn($code) => !$code instanceof CurrencyCode))) {
                throw new InternalException('Переданы не коды валют');
            }

            $currencies = $constraint->currencies;
        }

        if (count(array_filter($currencies, fn(CurrencyCode $code) => $code->value !== $value['currency']))) {
            $this->context->addViolation('Указанная валюта не поддерживается');

            return null;
        }

        if (!is_numeric($value['amount'])) {
            $this->context->addViolation('Сумма указана неправильно');

            return null;
        }

        $amount = \App\Value\Amount\Amount::makeByWithVAT(
            TypeCheck::float($value['amount']),
            CurrencyCode::from($value['currency']),
        );

        if ($constraint->validate && $this->context->getViolations()->count() === 0) {
            if (!is_callable($constraint->validate)) {
                throw new InternalException('Невалидное поле: validate');
            }

            call_user_func($constraint->validate, $amount, $this->context);
        }

        return $amount;
    }
}