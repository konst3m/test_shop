<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;

/** @see ConditionsValidator */
class Conditions extends Constraint
{

    /**
     * @param callable|null $tap
     * @param array|null $groups
     * @param mixed|null $payload
     */
    public function __construct(
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }
}