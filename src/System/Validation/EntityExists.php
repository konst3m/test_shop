<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;

class EntityExists extends Constraint implements TapableConstraint
{

    public function __construct(
        public readonly string $entityClass,
        public readonly ?\App\Entity\User $owner = null,
        public readonly mixed $userResolver = null,
        public readonly mixed $tap = null,
        public readonly mixed $validate = null,
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }

    public function getTap(): ?callable
    {
        return $this->tap;
    }
}