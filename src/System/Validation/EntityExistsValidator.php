<?php

declare(strict_types = 1);

namespace App\System\Validation;

use App\Exception\InternalException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;

use function Sentry\captureMessage;

class EntityExistsValidator extends AbstractValidator
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @param mixed        $value
     * @param EntityExists $constraint
     * @return object
     * @throws InternalException
     */
    public function run(mixed $value, Constraint $constraint): mixed
    {
        if (!isset($value)) {
            return null;
        }

        $repository = $this->entityManager->getRepository($constraint->entityClass);

        if (!$entity = $repository->find($value)) {
            $this->context->addViolation('Не найдено');

            return null;
        }

        if ($constraint->owner) {
            $userResolver = $constraint->userResolver ?? fn($entity) => $entity->getUser();
            /** @var \App\Entity\User|null $entityUser */
            $entityUser = $userResolver($entity);

            if ($entityUser === null) {
                throw new InternalException(
                    "У сущности ожидается владелец, но юзер не нашелся: $value $constraint->entityClass",
                );
            }

            if (!$constraint->owner->isSame($entityUser)) {
                $message = 'Попытка обратиться к сущности, не принадлежащей юзеру:'
                    . " $constraint->entityClass:$value, {$constraint->owner->getId()}";
                captureMessage($message);
                $this->context->addViolation('Не найдено');
            }
        }

        if ($constraint->validate && $this->context->getViolations()->count() === 0) {
            if (!is_callable($constraint->validate)) {
                throw new InternalException('Невалидное поле: validate');
            }

            call_user_func($constraint->validate, $entity, $this->context);
        }

        return $entity;
    }
}