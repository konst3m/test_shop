<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;

/** @see PeriodValidator */
class Period extends Constraint
{

    public function __construct(
        public int $maxPeriodDays = 1000,
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }
}