<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Carbon\CarbonImmutable;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PeriodValidator extends ConstraintValidator
{

    private const MAX_DIFF_YEARS = 15;

    /**
     * @param mixed  $value
     * @param Period $constraint
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!isset($value)) {
            return;
        }

        if (
            !is_array($value)
            || !isset($value['from'], $value['to'])
            || !is_numeric($value['from'])
            || !is_numeric($value['to'])
        ) {
            $this->context->addViolation('Передан неправильный период');

            return;
        }

        $from = CarbonImmutable::createFromTimestamp($value['from']);
        if (abs($from->diffInYears(CarbonImmutable::now())) > self::MAX_DIFF_YEARS) {
            $this->context->addViolation(
                'Начало периода отличается от сегодня более чем на ' . self::MAX_DIFF_YEARS . ' лет',
            );

            return;
        }

        $to = CarbonImmutable::createFromTimestamp($value['to']);
        if (abs($to->diffInYears(CarbonImmutable::now())) > self::MAX_DIFF_YEARS) {
            $this->context->addViolation(
                'Конец периода отличается от сегодня более чем на ' . self::MAX_DIFF_YEARS . ' лет',
            );

            return;
        }

        if ($from->greaterThan($to)) {
            $this->context->addViolation(
                'Начало периода должно быть раньше окончания',
            );

            return;
        }

        if (abs($from->diffInDays($to)) > $constraint->maxPeriodDays) {
            $this->context->addViolation(
                "Максимальный период $constraint->maxPeriodDays дней",
            );

            return;
        }
    }
}