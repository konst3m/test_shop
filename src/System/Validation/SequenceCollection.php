<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraints\Composite;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Required;

use function count;
use function is_array;

class SequenceCollection extends Composite
{

    public const MISSING_FIELD_ERROR = '2fa2158c-2a7f-484b-98aa-975522539ff8';
    public const NO_SUCH_FIELD_ERROR = '7703c766-b5d5-4cef-ace7-ae0dd82304e9';

    protected const ERROR_NAMES = [
        self::MISSING_FIELD_ERROR => 'MISSING_FIELD_ERROR',
        self::NO_SUCH_FIELD_ERROR => 'NO_SUCH_FIELD_ERROR',
    ];

    /** @deprecated since Symfony 6.1, use const ERROR_NAMES instead */
    protected static $errorNames = self::ERROR_NAMES;

    public array $fields = [];
    public mixed $allowExtraFields = false;
    public mixed $allowMissingFields = false;
    public mixed $extraFieldsMessage = 'This field was not expected.';
    public mixed $missingFieldsMessage = 'This field is missing.';

    public function __construct(
        array $fields,
        array $groups = null,
        mixed $payload = null,
        bool $allowExtraFields = null,
        bool $allowMissingFields = null,
        string $extraFieldsMessage = null,
        string $missingFieldsMessage = null,
    ) {
        parent::__construct(['fields' => $fields], $groups, $payload);

        $this->allowExtraFields = $allowExtraFields ?? $this->allowExtraFields;
        $this->allowMissingFields = $allowMissingFields ?? $this->allowMissingFields;
        $this->extraFieldsMessage = $extraFieldsMessage ?? $this->extraFieldsMessage;
        $this->missingFieldsMessage = $missingFieldsMessage ?? $this->missingFieldsMessage;
    }

    public function getRequiredOptions(): array
    {
        return ['fields'];
    }

    /** @return void */
    protected function initializeNestedConstraints(): void
    {
        parent::initializeNestedConstraints();

        foreach ($this->fields as $fieldName => $field) {
            // the XmlFileLoader and YamlFileLoader pass the field Optional
            // and Required constraint as an array with exactly one element
            if (is_array($field) && 1 === count($field)) {
                $this->fields[$fieldName] = $field = $field[0];
            }

            if (!$field instanceof Optional && !$field instanceof Required) {
                $this->fields[$fieldName] = new Required($field);
            }
        }
    }

    protected function getCompositeOption(): string
    {
        return 'fields';
    }
}
