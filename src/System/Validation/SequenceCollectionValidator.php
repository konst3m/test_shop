<?php

declare(strict_types = 1);

namespace App\System\Validation;

use ArrayAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Traversable;

use function array_key_exists;
use function count;
use function is_array;

class SequenceCollectionValidator extends ConstraintValidator
{

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof SequenceCollection) {
            throw new UnexpectedTypeException($constraint, SequenceCollection::class);
        }

        if (null === $value) {
            return;
        }

        if (!is_array($value) && !($value instanceof Traversable && $value instanceof ArrayAccess)) {
            throw new UnexpectedValueException($value, 'array|(Traversable&ArrayAccess)');
        }

        $context = $this->context;

        foreach ($constraint->fields as $field => $fieldConstraint) {
            $existsInArray = is_array($value) && array_key_exists($field, $value);
            $existsInArrayAccess = $value instanceof ArrayAccess && $value->offsetExists($field);

            if ($existsInArray || $existsInArrayAccess) {
                if (count($fieldConstraint->constraints) > 0) {
                    $violationsCount = $context->getValidator()
                        ->inContext($context)
                        ->atPath('[' . $field . ']')
                        ->validate($value[$field], $fieldConstraint->constraints)
                        ->getViolations()->count();

                    if ($violationsCount) {
                        return;
                    }
                }
            } elseif (!$fieldConstraint instanceof Optional && !$constraint->allowMissingFields) {
                $context->buildViolation($constraint->missingFieldsMessage)
                    ->atPath('[' . $field . ']')
                    ->setParameter('{{ field }}', $this->formatValue($field))
                    ->setInvalidValue(null)
                    ->setCode(SequenceCollection::MISSING_FIELD_ERROR)
                    ->addViolation();
            }
        }

        if (!$constraint->allowExtraFields) {
            foreach ($value as $field => $fieldValue) {
                if (!isset($constraint->fields[$field])) {
                    $context->buildViolation($constraint->extraFieldsMessage)
                        ->atPath('[' . $field . ']')
                        ->setParameter('{{ field }}', $this->formatValue($field))
                        ->setInvalidValue($fieldValue)
                        ->setCode(SequenceCollection::NO_SUCH_FIELD_ERROR)
                        ->addViolation();
                }
            }
        }
    }
}
