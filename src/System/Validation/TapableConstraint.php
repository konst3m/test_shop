<?php

declare(strict_types = 1);

namespace App\System\Validation;

interface TapableConstraint
{

    public function getTap(): ?callable;
}