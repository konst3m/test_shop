<?php

declare(strict_types = 1);

namespace App\System\Validation;

use Symfony\Component\Validator\Constraint;

/** @see UserValidator */
class User extends Constraint implements TapableConstraint
{

    /** @var int Этот юзер является рефералом 1 уровня текущего */
    public const MODE_IS_REFERRAL = 1 ** 2;
    /** @var int Этот юзер находится в ветке текущего юзера */
    public const MODE_IS_IN_BRANCH = 2 ** 2;

    public function __construct(
        public readonly int $mode = 0,
        public readonly mixed $tap = null,
        array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }

    public function getTap(): ?callable
    {
        return $this->tap;
    }
}