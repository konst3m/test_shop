<?php

declare(strict_types = 1);

namespace App\System\Validation;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;

class UserValidator extends AbstractValidator
{

    public function __construct(
        private readonly UserRepository $userRepository,

    ) {
    }

    /**
     * @param mixed $allConditions
     * @param User  $constraint
     * @return void
     */
    public function run(mixed $value, Constraint $constraint): mixed
    {
        if (!$user = $this->userRepository->findOneBy(['id' => $value])) {
            $this->context->addViolation('Пользователь не найден');

            return null;
        }

        return $user;
    }
}