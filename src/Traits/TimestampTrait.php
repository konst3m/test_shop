<?php

declare(strict_types = 1);

namespace App\Traits;

use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;

trait TimestampTrait
{

    /** @Gedmo\Timestampable(on="create") */
    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    private CarbonImmutable $createdAt;

    /** @Gedmo\Timestampable(on="update") */
    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: false)]
    private CarbonImmutable $updatedAt;

    public function setCreatedAt(CarbonImmutable $createdAt): self
    {
        $this->createdAt = CarbonImmutable::instance($createdAt);

        return $this;
    }

    public function getCreatedAt(): CarbonImmutable
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(CarbonImmutable $updatedAt): self
    {
        $this->updatedAt = CarbonImmutable::instance($updatedAt);

        return $this;
    }

    public function getUpdatedAt(): CarbonImmutable
    {
        return $this->updatedAt;
    }

    public function touch(): self
    {
        $this->setUpdatedAt(CarbonImmutable::now());

        return $this;
    }
}