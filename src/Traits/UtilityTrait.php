<?php

declare(strict_types = 1);

namespace App\Traits;

use Carbon\CarbonImmutable;
use Exception;

trait UtilityTrait
{

    /**
     * @throws Exception
     */
    public static function generateReportNumber(): string {
        $name = self::randomHex(random_int(5, 20));
        return CarbonImmutable::now()->toDateString().'-'. $name;
    }

    /**
     * @throws Exception
     */
    public static function randomHex(int $bytes = 8): string
    {
        return bin2hex(random_bytes($bytes));
    }
}