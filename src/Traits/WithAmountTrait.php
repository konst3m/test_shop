<?php

declare(strict_types = 1);

namespace App\Traits;

use App\Exception\InternalException;
use App\Value\Amount\Amount;
use App\Value\CurrencyCode;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait WithAmountTrait
{

    #[ORM\Column(type: Types::FLOAT, precision: 15, scale: 2)]
    private float $amount;

    #[ORM\Column(type: Types::STRING, length: 10, enumType: CurrencyCode::class)]
    private CurrencyCode $currencyCode;

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    public function getAmount(): Amount
    {
        return Amount::makeByWithVAT($this->amount, $this->currencyCode);
    }

    /**
     * @throws InternalException
     * @throws RoundingNecessaryException
     */
    public function setAmount(Amount $amount): self
    {
        if (!isset($this->currencyCode)) {
            $this->currencyCode = $amount->getCurrency();
        }

        if ($amount->getCurrency() !== $this->currencyCode) {
            throw new InternalException(
                "Не сходятся валюты: {$amount->getCurrency()->value} !== {$this->currencyCode->value}",
            );
        }
        $this->amount = $amount->getWithVAT();

        return $this;
    }
}