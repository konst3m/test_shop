<?php

declare(strict_types = 1);

namespace App\Value\Amount;

use App\Helper\NDS;
use App\Value\CurrencyCode;
use Brick\Math\BigDecimal;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Math\RoundingMode;

class Amount
{

    use AmountActionsTrait;

    /**
     * @throws NumberFormatException
     * @throws DivisionByZeroException
     */
    public static function makeByWithVAT(float $amount, CurrencyCode $currency): self
    {
        return new static($amount, $currency);
    }

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    public static function empty(CurrencyCode $currency): self
    {
        return new static(0, $currency);
    }

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    public static function makeByWithoutVAT(float $amount, CurrencyCode $currency): self
    {
        return new static(NDS::appendNDS($amount), $currency);
    }

    /**
     * @throws NumberFormatException
     * @throws DivisionByZeroException
     */
    protected static function make(BigDecimal $amount, CurrencyCode $currencyCode): self
    {
        return new static($amount, $currencyCode);
    }

    protected BigDecimal $amount;

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    protected function __construct(
        float|BigDecimal $amount,
        protected CurrencyCode $currency,
    ) {
        $this->amount = BigDecimal::of($amount);
    }

    /**
     * @throws DivisionByZeroException
     * @throws RoundingNecessaryException
     * @throws NumberFormatException
     */
    public function asWithoutVAT(): self
    {
        return self::makeByWithVAT($this->getWithoutVAT(), $this->currency);
    }

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    public function forkAmount(float $amount): Amount
    {
        return new static($amount, $this->getCurrency());
    }

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     */
    public function forkCurrency(CurrencyCode $currencyCode): Amount
    {
        return new static($this->getWithVAT(), $currencyCode);
    }

    public function getWithVATShrink(int $precision = 2): float
    {
        $shrinkValue = 10 ** $precision;

        return floor($this->amount->toFloat() * $shrinkValue) / $shrinkValue;
    }

    /**
     * @throws RoundingNecessaryException
     */
    public function getWithVAT(): float
    {
        return $this->amount->toScale(2, RoundingMode::HALF_UP)->toFloat();
    }

    /**
     * @throws RoundingNecessaryException
     */
    public function getWithoutVAT(): float
    {
        return round(NDS::subtractNDS($this->getWithVAT()), 2);
    }

    public function getCurrency(): CurrencyCode
    {
        return $this->currency;
    }

    /**
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     */
    public function clone(): Amount
    {
        return new static($this->amount, $this->currency);
    }

    protected function getAmount(): BigDecimal
    {
        return $this->amount;
    }
}
