<?php

declare(strict_types = 1);

namespace App\Value\Amount;

use App\Exception\InternalException;
use Brick\Math\RoundingMode;

trait AmountActionsTrait
{

    abstract public function getCurrency(): \App\Value\CurrencyCode;

    public function multiplyBy(float $multiplier): Amount
    {
        return static::make($this->getAmount()->multipliedBy($multiplier), $this->getCurrency());
    }

    public function divideBy(float $divider): Amount
    {
        return static::make($this->getAmount()->dividedBy($divider, 2, RoundingMode::HALF_UP), $this->getCurrency());
    }

    public function minus(Amount $amount): Amount
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->validateCurrency($amount);

        return static::make($this->getAmount()->minus($amount->getAmount()), $this->getCurrency());
    }

    public function plus(Amount $amount): Amount
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->validateCurrency($amount);

        return static::make($this->getAmount()->plus($amount->getAmount()), $this->getCurrency());
    }

    public function abs(): self
    {
        return static::make($this->getAmount()->abs(), $this->getCurrency());
    }

    public function isEqual(Amount $amount): bool
    {
        return $this->getCurrency() === $amount->getCurrency()
            && $this->getAmount()->isEqualTo($amount->getAmount());
    }

    public function isNotEqual(Amount $amount): bool
    {
        return !$this->isEqual($amount);
    }

    public function positive(): Amount
    {
        return $this->abs();
    }

    public function negative(): Amount
    {
        if ($this->getAmount()->isNegative()) {
            return static::make($this->getAmount(), $this->getCurrency());
        }

        return static::make($this->getAmount()->negated(), $this->getCurrency());
    }

    public function percent(float $percent): Amount
    {
        return static::make($this->getAmount()->multipliedBy($percent / 100), $this->getCurrency());
    }

    public function min(Amount $amount): Amount
    {
        return static::makeByWithVAT(min($amount->getWithVAT(), $this->getWithVAT()), $this->getCurrency());
    }

    public function isNegative(): bool
    {
        return $this->getAmount()->isNegative();
    }

    public function isZero(): bool
    {
        return $this->getAmount()->isZero();
    }

    public function gte(Amount $amount): bool
    {
        $this->validateCurrency($amount);

        return $this->getAmount()->isGreaterThanOrEqualTo($amount->getAmount());
    }

    public function gt(Amount $amount): bool
    {
        $this->validateCurrency($amount);

        return $this->getAmount()->isGreaterThan($amount->getAmount());
    }

    public function lte(Amount $amount): bool
    {
        $this->validateCurrency($amount);

        return $this->getAmount()->isLessThanOrEqualTo($amount->getAmount());
    }

    public function lt(Amount $amount): bool
    {
        $this->validateCurrency($amount);

        return $this->getAmount()->isLessThan($amount->getAmount());
    }

    public function isPositiveOrZero(): bool
    {
        return $this->getAmount()->isPositiveOrZero();
    }

    public function isNegativeOrZero(): bool
    {
        return $this->getAmount()->isNegativeOrZero();
    }

    public function isPositive(): bool
    {
        return $this->getAmount()->isPositive();
    }

    public function validateCurrency(Amount $amount): void
    {
        if (!$this->checkCurrency($amount->getCurrency())) {
            $currencies = "{$this->getCurrency()->value} != {$amount->getCurrency()->value}";
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new InternalException("Нельзя совершать действия с объемами в разных валютах: $currencies");
        }
    }

    private function checkCurrency(\App\Value\CurrencyCode $currencyCode): bool
    {
        return $this->getCurrency() === $currencyCode;
    }

}