<?php

declare(strict_types = 1);

namespace App\Value;

use App\Exception\InternalException;
use App\Value\Amount\Amount;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;

enum CurrencyCode: string
{

    case RUB = 'RUB';
    case USD = 'USD';
    case EUR = 'EUR';

    public const ALL = [
        self::RUB,
        self::USD,
        self::EUR,
    ];

    private const ISO_MAP = [
        643 => self::RUB,
        840 => self::USD,
        978 => self::EUR,
    ];

    public static function getByISO(int $code): self
    {
        return self::ISO_MAP[$code];
    }

    public static function getValues(): array
    {
        return array_values(array_map(fn(self $code) => $code->value, self::ALL));
    }

    /** @throws InternalException */
    public static function getISO(self $searchCode): int
    {
        foreach (self::ISO_MAP as $iso => $code) {
            if ($code === $searchCode) {
                return $iso;
            }
        }

        throw new InternalException("Не найден ISO код для $searchCode->value");
    }

    /** @throws InternalException */
    public function toISO(): int
    {
        return self::getISO($this);
    }

    /**
     * @throws NumberFormatException
     * @throws DivisionByZeroException
     */
    public function makeAmount(float $amount): Amount
    {
        return Amount::makeByWithVAT($amount, $this);
    }
}