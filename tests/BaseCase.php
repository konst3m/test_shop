<?php
declare(strict_types = 1);

namespace App\Tests;

use App\Tests\Traits\AppTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class BaseCase extends WebTestCase
{
    use AppTrait;

    protected EntityManagerInterface $entityManager;
    protected KernelInterface $kernel2;
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        // Запуск ядра Symfony
        $this->kernel2 = self::bootKernel();
        $container = $this->kernel2->getContainer();
        // Получение EntityManager из контейнера
        $this->entityManager = $container->get('doctrine')->getManager();
    }
}