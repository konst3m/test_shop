<?php
declare(strict_types = 1);

namespace App\Tests\Controllers;

use App\Entity\Enum\DeliveryType;
use App\Entity\Enum\OrderStatus;
use App\Entity\Product;
use App\Entity\ShopOrder;
use App\Exception\InternalException;
use App\Http\RequestDTO\CartRequestDTO;
use App\Service\CartService;
use App\Tests\BaseCase;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Throwable;

class CartTest extends BaseCase
{

    /**
     * @throws Throwable
     */
    public function testGetCart(): void
    {
        $user = self::createRandomUser();
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->client->request('POST', '/api/get-cart');
        $response = $this->client->getResponse();
        $content = $response->getContent();
        $decodeContent = self::jsonDecode($content);

        self::assertSame(200, $response->getStatusCode());
    }

    /**
     * @throws Throwable
     */
    public function testAddToCart(): void
    {
        $user = self::createRandomUser();
        $product = self::createRandomProduct();

        $this->entityManager->persist($product);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->client->request(
            'POST',
            '/api/add-to-cart',
            [
                'product_id' => $product->id,
                'user_id'    => $user->id,
            ],
        );
        $response = $this->client->getResponse();

        $cartService = self::getFromContainer(CartService::class);
        $cart = $cartService->getOrCreateCart($user);

        /** @var ShopOrder $order */
        $order = $cart->orders[0];
        /** @var Product $productInCart */
        $productInCart = $order->products[0];

        self::assertSame($productInCart->id, $product->id);
        self::assertSame(200, $response->getStatusCode());

        $cartService->dropCart($user);
    }

    /**
     * @throws Throwable
     * @throws RoundingNecessaryException
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     * @throws InternalException
     */
    public function testBuyCart(): void
    {
        $user = self::createRandomUser();
        $product = self::createRandomProduct();

        $this->entityManager->persist($product);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $cartService = self::getFromContainer(CartService::class);
        $dto = new CartRequestDTO($product, $user);
        $cartService->addToCart($dto);

        $this->client->request(
            'POST',
            '/api/buy-cart',
            [
                'id'            => $user->id,
                'phone'         => self::randomPhone(),
                'delivery_type' => DeliveryType::COURIER->value,
                'user_id'       => $user->id,
            ],
        );
        $response = $this->client->getResponse();

        $newOrder = self::getLastEntity(ShopOrder::class);
        $cart = $cartService->getOrCreateCart($user);

        /** @var ShopOrder $paidOrder */
        $paidOrder = $cart->orders[0];
        /** @var Product $paidProduct */
        $paidProduct = $paidOrder->products[0];

        self::assertSame(OrderStatus::NEW, $newOrder->orderStatus);
        self::assertSame($paidProduct->getId(), $product->getId());
    }
}