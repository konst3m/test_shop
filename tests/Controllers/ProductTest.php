<?php
declare(strict_types = 1);

namespace App\Tests\Controllers;

use App\Entity\Product;
use App\Tests\BaseCase;
use Throwable;

class ProductTest extends BaseCase
{

    /**
     * @throws Throwable
     */
    public function testOrderList(): void {
        $user = self::createRandomUser();
        $product = self::createRandomProduct();

        $this->entityManager->persist($user);
        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $this->client->request('POST', '/api/list-product');

        $response = $this->client->getResponse();
        $data = self::jsonDecode($response->getContent());
        self::assertSame('success', $data['status']);
        self::assertNotEmpty($data['data']);
    }

    /**
     * @throws Throwable
     */
    public function testCreateOrderController(): void
    {
        $user = self::createRandomUser();
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $requestData = [
            'name' => self::randomString(),
            'measurements' => [
                'weight' => self::randomInt(),
                'height' => self::randomInt(),
                'width' => self::randomInt(),
                'length' => self::randomInt(),
            ],
            'description' => self::randomString(),
            'cost' => self::randomInt(),
            'tax' => self::randomInt(),
            'version' => self::randomInt(),
        ];

        $this->client->request('POST', '/api/create-product', $requestData);
        $response = $this->client->getResponse();
        $responseData = self::jsonDecode($response->getContent());

        $product = self::getLastEntity(Product::class);

        self::assertSame('success', $responseData['status']);
        self::assertSame($requestData['name'], $responseData['data']['name']);
        self::assertSame($requestData['name'], $product->name);
    }
}