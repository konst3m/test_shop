<?php
declare(strict_types = 1);

namespace App\Tests\Controllers;

use App\Entity\Cart;
use App\Entity\Enum\OrderStatus;
use App\Exception\InternalException;
use App\Tests\BaseCase;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Throwable;

class ReportTest extends BaseCase
{

    /**
     * @throws Throwable
     * @throws RoundingNecessaryException
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     * @throws InternalException
     */
    public function testCreateReport(): void {

        $user = self::createRandomUser();
        $product = self::createRandomProduct();
        $cart = new Cart($user);
        $order = self::createRandomOrder($user);
        $order->orderStatus = OrderStatus::DONE;
        $order->addProduct($product);
        $cart->addShopOrder($order);

        $this->entityManager->persist($user);
        $this->entityManager->persist($product);
        $this->entityManager->persist($order);
        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        $this->client->request(
            'POST',
            '/api/create-report',
        );
        $this->client->getResponse();
    }

}