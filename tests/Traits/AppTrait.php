<?php

declare(strict_types = 1);

namespace App\Tests\Traits;

use App\Entity\Enum\OrderStatus;
use App\Entity\Product;
use App\Entity\ShopOrder;
use App\Entity\User;
use App\Exception\InternalException;
use App\Http\RequestDTO\ProductRequestDTO;
use App\Value\Amount\Amount;
use App\Value\CurrencyCode;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Carbon\CarbonImmutable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Throwable;

/**
 * Методы для создания сущностей
 */
trait AppTrait
{

    use UtilityTrait;

    /** @throws Throwable */
    protected static function createRandomUser(
        string $name = null,
        string $email = null,
        string $phone = null,
        string $password = null,
        array $roles = [User::ROLE_API],
    ): User {
        $user = new User();
        $user->name = $name ?? self::randomString();
        $user->email = $email ?? self::randomEmail();
        $user->password = $password ?? self::randomHex();
        $user->phone = $phone ?? self::randomPhone();
        $user->roles = $roles;
        $user->setCreatedAt(CarbonImmutable::now());
        $user->setUpdatedAt(CarbonImmutable::now());

        return $user;
    }

    /**
     * @throws RoundingNecessaryException
     * @throws DivisionByZeroException
     * @throws NumberFormatException
     * @throws InternalException
     * @throws Throwable
     */
    protected static function createRandomProduct(
        string $name = null,
        string $description = null,
        float $cost = null,
        float $tax = null,
        int $version = null,
        float $weight = null,
        float $height = null,
        float $width = null,
        float $length = null,
    ): Product {
        $product = new Product();
        $cost = $cost ?? self::randomFloat();
        $amount = Amount::makeByWithVAT($cost, CurrencyCode::RUB);
        $product->name = $description ?? self::randomString();
        $product->description = $name ?? self::randomString();
        $product->height = $height ?? self::randomFloat();
        $product->weight = $weight ?? self::randomFloat();
        $product->length = $length ?? self::randomFloat();
        $product->width = $width ?? self::randomFloat();
        $product->version = $version ?? self::randomInt();
        $product->tax = $tax ?? self::randomFloat();
        $product->setAmount($amount);
        $product->setCreatedAt(CarbonImmutable::now());
        $product->setUpdatedAt(CarbonImmutable::now());

        return $product;
    }

    protected static function createRandomOrder(User $user): ShopOrder
    {
        $order = new ShopOrder($user);
        $order->orderStatus = OrderStatus::NEW;
        $order->setUpdatedAt(CarbonImmutable::now());
        $order->setCreatedAt(CarbonImmutable::now());

        return $order;
    }

    protected static function createCart(User $user): ShopOrder
    {
        $order = new ShopOrder($user);
        $order->orderStatus = OrderStatus::NEW;
        $order->setUpdatedAt(CarbonImmutable::now());
        $order->setCreatedAt(CarbonImmutable::now());

        return $order;
    }

    /**
     * @param class-string<T> $entity
     * @param array           $findBy
     * @return T|null
     *
     * @template T
     */
    protected static function getLastEntity(string $entity, array $findBy = []): ?object
    {
        return self::getEntityManager()
            ->getRepository($entity)
            ->findOneBy($findBy, ['id' => 'DESC']);
    }

    /**
     * Возвращает EntityManager доктрины
     *
     * @return EntityManager
     */
    protected static function getEntityManager(): EntityManager
    {
        return self::getFromContainer('doctrine.orm.default_entity_manager');
    }

    /**
     * @param class-string<T> $value
     * @return T
     *
     * @template T
     */
    protected static function getFromContainer(string $value): ?object
    {
        if (empty(self::$kernel)) {
            self::rebuildKernel();
        }

        return self::getContainer()->get($value);
    }

    /**
     * Пересоздает kernel
     *
     * @param string[] $options
     * @return KernelInterface
     */
    private static function rebuildKernel($options = ['environment' => 'test']): KernelInterface
    {
        return static::bootKernel($options);
    }
}