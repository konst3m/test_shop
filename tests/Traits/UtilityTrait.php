<?php

declare(strict_types = 1);

namespace App\Tests\Traits;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use RuntimeException;
use Throwable;

/**
 * Дополнительные утилитарные методы
 */
trait UtilityTrait
{

    /**
     * Возвращает плоский массив с разделителем "."(точка) в ключах
     *
     * @param array $incomeArray
     * @return array
     */
    public static function flattenArray(array $incomeArray): array
    {
        return self::recursiveFlatten($incomeArray);
    }

    public static function getFromArray(array $incomeArray, string $key)
    {
        $flattenArray = self::flattenArray($incomeArray);

        return $flattenArray[$key] ?? null;
    }

    /**
     * Исключает ключи из массива
     *
     * @param array $incomeArray
     * @param array $keysToExclude
     * @return array
     */
    public static function excludeKeys(array $incomeArray, array $keysToExclude): array
    {
        $result = [];
        foreach ($incomeArray as $key => $value) {
            if (!in_array($key, $keysToExclude, true)) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Загружает файлы фикстур
     *
     * @return array
     */
    public static function loadFixtures(string $fileName, object $object): array
    {
        $filePath = (new \ReflectionClass($object::class))->getFileName();

        $fixturePath = dirname($filePath) . DIRECTORY_SEPARATOR . 'fixtures' . DIRECTORY_SEPARATOR . $fileName . '.php';
        if (!is_file($fixturePath)) {
            throw new RuntimeException("Фикстуры не существует: {$fixturePath}");
        }

        /** @noinspection PhpIncludeInspection */
        return include $fixturePath;
    }

    /**
     * Возвращает случайную строку с символами 0-F,
     * длинной от 10 до 40 символов
     *
     * @return string
     * @throws Throwable
     */
    public static function randomString(int $lengthBytes = null): string
    {
        return self::randomHex($lengthBytes ?? random_int(5, 20));
    }

    /**
     * Возвращает случайную строку с символами 0-F
     *
     * @param int $bytes Количество байт. 1 байт = 2 символа
     * @return string
     * @throws Throwable
     */
    public static function randomHex(int $bytes = 8): string
    {
        return bin2hex(random_bytes($bytes));
    }

    /** @throws Throwable */
    public static function randomInt(int $min = 0, int $max = 2147483647): int
    {
        return random_int($min, $max);
    }

    /** @throws Throwable */
    public static function randomFloat(int $min = 0, int $max = 2147483647, int $fraction = 2): float
    {
        $multiplicator = 10 ** $fraction;

        return (float) (self::randomInt($min * $multiplicator, $max * $multiplicator) / $multiplicator);
    }

    /**
     * Возвращает случайный номер телефона
     *
     * @param string $code
     * @param int    $length
     * @return string
     *
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function randomPhone(string $code = '+7', int $length = 10): string
    {
        return $code . random_int((int) str_pad('', $length, '1'), (int) str_pad('', $length, '9'));
    }

    /**
     * Возвращает случайный адрес почты
     *
     * @return string
     * @throws Throwable
     */
    public static function randomEmail(string $domain = 'example.com'): string
    {
        return sprintf("%s@{$domain}", self::randomString());
    }

    /**
     * @param bool $numeric
     * @return array
     * @throws Throwable
     */
    public static function randomArray(bool $numeric = false): array
    {
        $result = [];

        $itemsCount = random_int(3, 10);
        for ($i = 0; $i < $itemsCount; $i++) {
            if ($numeric) {
                $result[] = self::randomString();
            } else {
                $result[self::randomHex()] = self::randomString();
            }
        }

        return $result;
    }

    public static function REQ(string $uri, string $method = 'GET'): Request
    {
        return new Request($method, $uri);
    }

    /**
     * Возвращает Response
     *
     * @param string $body
     * @param int    $code
     * @param array  $headers
     * @return Response
     */
    public static function RES(string $body, int $code = 200, array $headers = []): Response
    {
        return new Response($code, $headers, $body);
    }

    /**
     * Возвращает ошибку при запросе
     *
     * @param string       $message
     * @param Request|null $request
     * @return RequestException
     */
    public static function ERR(string $message = 'DEFAULT TEST ERROR', Request $request = null): RequestException
    {
        return new RequestException($message, $request ?? self::REQ('test'));
    }

    /**
     * Json Response
     *
     * @param array $body
     * @param int   $code
     * @param array $headers
     * @return Response
     * @throws Throwable
     */
    public static function jRES(array $body, int $code = 200, array $headers = []): Response
    {
        return self::RES(self::jsonEncode($body), $code, $headers);
    }

    /**
     * @param string $data
     * @return array
     * @throws Throwable
     */
    public static function jsonDecode(string $data): array
    {
        return \App\Helper\Json::decode($data, options: JSON_PRESERVE_ZERO_FRACTION);
    }

    /**
     * @param array $data
     * @return string
     * @throws Throwable
     */
    public static function jsonEncode(array $data): string
    {
        return \App\Helper\Json::encode($data);
    }

    /**
     * @param array $incomeArray
     * @param array $prefix
     * @return array
     */
    private static function recursiveFlatten(array $incomeArray, array $prefix = []): array
    {
        $result = [];
        if (empty($prefix)) {
            $prefixString = '';
        } else {
            $prefixString = implode('.', $prefix) . '.';
        }
        foreach ($incomeArray as $key => $value) {
            if (is_array($value)) {
                if (empty($value)) {
                    $result["{$prefixString}{$key}"] = $value;
                    continue;
                }

                /** @noinspection SlowArrayOperationsInLoopInspection */
                $result = array_merge($result, self::recursiveFlatten($value, array_merge($prefix, [$key])));
            } elseif (is_string($value) || is_int($value) || is_float($value) || is_bool($value) || is_null($value)) {
                $result["{$prefixString}{$key}"] = $value;
            } else {
                throw new RuntimeException('Невалидный тип: ' . gettype($value));
            }
        }

        return $result;
    }
}